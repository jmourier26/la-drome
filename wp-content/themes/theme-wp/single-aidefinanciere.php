<?php
$terms = get_the_terms(get_the_ID(), 'taxaidefinanciere');
$options = get_option('options');
if(!get_field("accessible"))
{
    header('Location: '.get_term_link($terms[0], "taxaidefinanciere"));
}

get_header();

$term_parent = get_term($terms[0]->parent);
$entete = get_the_post_thumbnail_url() != ""? get_the_post_thumbnail_url() : asset_url("img/default.jpg");
?>
    <section class="page-article">
        <div class="btnretour">
            <i class="icon-full_arrow_left"></i>
            <a href="<?= get_the_permalink($options['lien_aide'])?>">Retour</a>
        </div>

        <div class="ariane d-flex flex-wrap align-items-center">
            <a href="index.php">Accueil</a>
            <span class="arrow">></span>
            <a href="<?= get_the_permalink($options['lien_aide_financiere'])?> ">Aide financière</a>
            <span class="arrow">></span>
            <a href="<?= get_the_permalink($options['lien_aide'])?> ">Aides départementales aux collectivités et aux tiers</a>
            <span class="arrow">>lien_aide
            <a href="<?= get_the_permalink($options['lien_aide'])."#".$term_parent->slug?> "><?= $term_parent->name?></a>
            <span class="arrow">></span>
            <a href="<?= get_term_link($terms[0])?> "><?= $terms[0]->name?></a>
            <span class="arrow">></span>
            <p><?= get_the_title()?></p>
        </div>

        <div class="topfiche article" style="background-image: url('<?= $entete?>')">
            <div class="bloctitre">
                <p class="cat catentete cat-clt centeredtext upper white">culture</p>
                <h1 class="upper thinh1"><?= get_the_title()?></h1>
                <p class="paraf">Modifié le <?= get_the_modified_date()?></p>
            </div>
        </div>

        <style>
            h3{
                text-align: center;
            }
        </style>

        <div class="contenu-article cms">

            <?= get_the_content()?>

        </div>
        <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
    </section>

<?php

get_footer();

?>