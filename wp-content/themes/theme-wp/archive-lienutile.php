<?php
get_header();
$options = get_option('options');

$args = ( array(
    'post_type' => "lienutile",
    'order' => 'ASC',
    'orderby' => 'date',
) );

query_posts($args);

if(get_field('image', 'cpt_lienutile_gestion_de_la_page_liens_utiles') == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_field('image', 'cpt_lienutile_gestion_de_la_page_liens_utiles');
}

?>
<section class="liens-utiles">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche"  style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_field('titre', 'cpt_lienutile_gestion_de_la_page_liens_utiles');?></h1>
            <div class="paraf bolder">
                <?= apply_filters("the_content",get_field('texte', 'cpt_lienutile_gestion_de_la_page_liens_utiles'));?>
            </div>
        </div>
    </div>
    <div class="liste-liens">
        <?php if(have_posts()):?>
            <?php
            while ( have_posts() ) : the_post(); ?>
                <div class="lien">
                    <p class="titrelien"><?php the_title()?></p>
                    <div class="d-flex flex-column flex-md-row">
                        <div class="imagelien" style="background-image: url('<?= get_the_post_thumbnail_url()?>')"></div>
                        <div class="desclien righttext">
                            <?php the_content()?>
                            <a href="<?= get_field("lien")?>" target="_blank" class="lienplus">En lire plus >></a>
                        </div>
                    </div>
                </div>
            <?php endWhile;
            wp_reset_postdata();?>
        <?php else:?>
            <p>Pas encore de lien utile</p>
        <?php endif;?>
    </div>

    <form id="newsletter">
        <div class="newsletter d-flex flex-wrap align-items-center justify-content-center white">
            <p>Inscrivez-vous <br><span>à notre newsletter</span></p>
            <span class="line"></span>
            <div class="containinput">
                <input class="input" id="emailnewsletter" type="email" >
                <label for="email" class="label white">Adresse email</label>
                <div class="row rgpd">
                    <div class="col-sm-12">
                        <label for="rgpd">J'accepte les termes et conditions de la <a href="<?= get_the_permalink(3)?>" title="Politique de confidentialité de la Drôme" target="_blank">Politique de Confidentialité</a>.
                            <input type="checkbox" name="rgpd" id="rgpd" value="accept">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <span class="message"></span>
            </div>
            <input type="hidden" id="action" value="subcribeMailPoet">
            <button type="submit" class="upper">inscription <i class="icon-full_arrow_right"></i></button>
        </div>
    </form>
</section>

<?php
get_footer();
?>


