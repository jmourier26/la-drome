<?php
/**
 * Created by PhpStorm.
 * User: Benoît
 * Date: 09/07/2016
 * Time: 12:11
 */
// Do not delete these lines
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Ne pas t&eacute;l&eacute;charger cette page directement, merci !');
if (!empty($post->post_password)) { // if there's a password
    if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {  // and it doesn't match the cookie
        ?>

        <h2><?php _e('Prot&eacute;g&eacute; par mot de passe'); ?></h2>
        <p><?php _e('Entrer le mot de passe pour voir les commentaires'); ?></p>

        <?php return;
    }
}
?>
<!-- Liste des Commentaires-->
<div id="lst-comments">
    <?php
    $my_walker_comment = new MY_Walker_Comment();

    wp_list_comments(
        array(
            'walker' => $my_walker_comment
        )
    );
    ?>
</div>
<?php if(is_user_logged_in()):?>
    <a href="#" class="btncom white centeredtext openComment">Laisser un commentaire</a>
<?php else: ?>
    <a href="<?= wpmem_login_url()."?redirect_to=".get_the_permalink() ?>" class="btncom white centeredtext">Se connecter</a>
<?php endif;?>
<div id="com-publish">
    <?php //boucle pour l'affichage du formulaire de commentaire
    if ('open' == $post->comment_status) : ?>
        <?php if ( get_option('comment_registration') && !$user_ID ) : ?>
            <p>Vous devez être connecté pour laisser un commentaire.</p>

        <?php else : ?>

            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
                <?php if ( $user_ID ) : ?>
                    <p>
                    Vous êtes connecté en tant que <?php echo $user_identity; ?>.<br>
                    <?= do_shortcode("[wpmem_logout]")?>
                    </p>
                <?php else : ?>

                    <p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="40" tabindex="1" />
                        <label for="author"><small>Nom <?php if ($req) echo "(requis)"; ?></small></label></p>

                    <p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="40" tabindex="2" />
                        <label for="email"><small>email (ne sera pas publi&eacute;) <?php if ($req) echo "(requis)"; ?></small></label></p>

                <?php endif; ?>

                <!--<p><small><strong>XHTML:</strong> <?php _e('Vous pouvez utiliser ces tags&#58;'); ?> <?php echo allowed_tags(); ?></small></p>-->

                <div class="form-group">
                    <textarea name="comment" id="comment" class="form-control texteditor" cols="60" rows="5" tabindex="4"></textarea>
                </div>

                <div style="text-align: center">
                    <input name="submit" type="submit" id="submit" tabindex="5" value="Envoyer" class="btncom white centeredtext"/>
                    <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
                    <?php if(isset($_GET['replytocom'])): ?>
                        <input type="hidden" name="comment_parent" value="<?php echo $_GET['replytocom']; ?>" />
                    <?php endif; ?>
                </div>

                <div style="clear:both"></div>

                <?php do_action('comment_form', $post->ID); ?>

            </form>

        <?php endif; // If registration required and not logged in ?>

    <?php endif; // if you delete this the sky will fall on your head ?>
</div>

<?php
/*comment_form(
        array(
            'class_form' => 'form-control'
        )
    );*/?>
<div style="clear:both"></div>
<!--<script src="--><?//= asset_url('js/tinymce/tinymce.min.js') ?><!--"></script>-->
<script>
    document.addEventListener('DOMContentLoaded', function () {

        if (isEmpty($('#lst-comments'))){
            $('#lst-comments').html("<p>Il n’y a pas encore de commentaires sur cet article</p>")
        }
    });

    function isEmpty( el ){
        return !$.trim(el.html())
    }

</script>