<?php

get_header();

$options = get_option("options");

$today = date('Ymd');
$year = date('Y');
$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;
if(isset($_GET['order']))
{
    $order = $_GET['order'];
    $args = array(
        'post_type' => 'datescles',
        'posts_per_page' => '8',
        'orderby' => 'meta_value',
        'meta_key'=> 'date',
        'order' => $order,
        'meta_query' => array(
            array(
                'key'		=> 'date',
                'compare'	=> '>=',
                'value'		=> $today,
            )
        ),
        'paged' => $paged
    );
}
else
{
    $args = array(
        'post_type' => 'datescles',
        'posts_per_page' => '8',
        'orderby' => 'meta_value',
        'meta_key'=> 'date',
        'order' => "ASC",
        'meta_query' => array(
            array(
                'key'		=> 'date',
                'compare'	=> '>=',
                'value'		=> $today,
            )
        ),
        'paged' => $paged
    );
}
$loop = new WP_Query($args);

if(get_field('image', 'cpt_datescles_gestion_de_la_page_dates_cl__s') == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_field('image', 'cpt_datescles_gestion_de_la_page_dates_cl__s');
}
?>

<section class="liste-assistance page_date">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_field('titre', 'cpt_datescles_gestion_de_la_page_dates_cl__s');?></h1>
            <div class="paraf">
                <?= get_field('texte', 'cpt_datescles_gestion_de_la_page_dates_cl__s');?>
            </div>
        </div>
    </div>
    <div class="liste-blocs-assistance">
        <div class="filtres">
            <form action="" method="get">
                <p>
                    Trier par :
                    <select name="order" id="order" onchange="this.form.submit()">
                        <option value="ASC" <?= (isset($_GET['order']) && $_GET['order'] == 'ASC')? "selected='selected'" : "" ?>>Date croissante</option>
                        <option value="DESC" <?= (isset($_GET['order']) && $_GET['order'] == 'DESC')? "selected='selected'" : "" ?>>Date décroissante</option>
                    </select>
                </p>
            </form>
        </div>
        <div class="datescles">
                <?php if($loop->have_posts()):?>
                    <div class="dates row">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                            <?php
                            if(get_the_post_thumbnail_url() == ""){

                                $image = asset_url("img/actu_base.jpg");
                            } else {
                                $image = get_the_post_thumbnail_url();
                            }
                            ?>
                            <div class="evenement col-sm-6 col-lg-4  col-xl-3 lefttext">
                                <div class="illustration one" style="background-image: url('<?= $image?>')"></div>
                                <p class="nomevent upper"><?php the_title()?></p>
                                <p class="lieuevent"><i class="icon-where"></i><?php the_field("lieu")?></p>
                                <p class="desc"><?= manual_excerpt(get_the_content(), 0, 100)?></p>
                                <a href="<?php the_permalink()?>">En lire plus <i class="icon-arrow_right"></i></a>
                                <p class="dateevent upper white"><?= date_i18n( "d F Y", strtotime( get_field("date") ) );?></p>
                            </div>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                <?php else:?>
                    <div class="">
                        <p>Pas d'évènement prévu</p>
                    </div>
                <?php endif;?>
            <div class="my-pagination container">
                <?php
                $args_page = array(
                    'base' => @add_query_arg('page','%#%'),
                    'format' => '?page=%#%',
                    'current' => max( 1, get_query_var('page') ),
                    'total' => $loop->max_num_pages,
                    'mid_size' => 2,
                    'prev_text' => '<i class="icomoon icon-full_arrow prev"></i>',
                    'next_text' => '<i class="icomoon icon-full_arrow next"></i>',
                    'type' => 'plain'
                );
                echo paginate_links( $args_page );
                ?>
            </div>
            </div>
        </div>
    </div>


    <div class="bandeaide d-flex flex-wrap align-items-center justify-content-center white">
        <p>Aide <span>financière</span></p>
        <p class="phrase"><?= $options['text_aides']?></p>
        <a target="_blank" href="<?= get_the_permalink($options['lien_aide'])?>" class="upper link">découvrir <i class="icon-full_arrow_right"></i></a>
    </div>
</section>

<?php

get_footer()
    ?>

