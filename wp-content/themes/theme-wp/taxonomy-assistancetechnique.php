<?php

$term = get_term(get_queried_object()->term_id, 'assistancetechnique');

if($term->parent == "")
{
    header('Location: '.get_the_permalink(26)."#".$term->slug);
}
$term_parent = get_term(get_queried_object()->parent);
$paged = ( get_query_var( 'page' ) ) ? get_query_var( 'page' ) : 1;

//RSS idcite

switch($term_parent->slug){

    case 'amenagement':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331409" );
        break;
    case 'tourisme-et-commerce':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331410" );
        break;
    case 'culture':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331411" );
        break;
    case 'environnement-eau':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331412" );
        break;
    case 'numerique':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331413" );
        break;
    case 'sante':
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/syndication.rss?r=5331422" );
        break;
    default:
        $feed = fetch_feed( "https://www.idcite-drome.fr/xml/" );

}

if( ! is_wp_error( $rss ) ){
    $rss_item = $feed->get_item(0);
}



get_header();
$options = get_option('options');

$args_service = array(
            'post_type' => 'service',
		    'posts_per_page' => '3',
            'tax_query' => array(
             array(
                 'taxonomy' => 'assistancetechnique',
                 'field' => 'slug',
                 'terms' => $term->slug
             )
            ),
		    'paged' => $paged
	    );
$loop_service = new WP_Query($args);

$args_docs = array(
    'post_type' => 'documentdereference',
    'posts_per_page' => '-1',
    'tax_query' => array(
        array(
            'taxonomy' => 'assistancetechnique',
            'field' => 'slug',
            'terms' => $term->slug
        )
    ),
);

$args_fiches = array(
    'post_type' => 'fichepratique',
    'posts_per_page' => '-1',
    'tax_query' => array(
        array(
            'taxonomy' => 'assistancetechnique',
            'field' => 'slug',
            'terms' => $term_parent->slug
        )
    ),
);

$argsArticle = array(
    'posts_per_page' => 2,
    'orderby' => 'date',
    'order' => "DESC",
);
$loop = new WP_Query($argsArticle);

$loop_service = new WP_Query($args_service);
$loop_docs  = new WP_Query($args_docs);
$fiches_pratique = get_posts($args_fiches);


$entete = get_field("image", $term) != ""? get_field("image", $term) : asset_url("img/entete.jpg");
?>

<section class="voirie">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_the_permalink($options['lien_assistance'])."#".$term_parent->slug?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $entete?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <p class="cat catentete cat-ame centeredtext upper white" style="background-color: <?= get_field("couleur", $term_parent)?>"><?= $term_parent->name?></p>
            <h1 class="upper thinh1"><?= $term->name?></h1>
                <div class="paraf">
                    <?=$term->description?>
                </div>
        </div>
    </div>

    <div class="contenuduo row centeredtext <?= ($loop_docs->have_posts()) ? '' : 'solo'?>">
        <div class="services <?= ($loop_docs->have_posts()) ? 'col-lg-6' : 'col-lg-12 serv'?>">
            <h2 class="upper">les <span>services proposés</span></h2>
            <?php if($loop_service->have_posts()):?>
                <?php
                 while ( $loop_service->have_posts() ) : $loop_service->the_post(); ?>
                <div class="liste-services d-flex flex-column">
                    <div class="service row">
                        <a href="<?= get_the_permalink()?>" class="image" style="background-image: url('<?= get_the_post_thumbnail_url()?>')"></a>
                        <div class="paragraphe">
                            <a href="<?= get_the_permalink()?>" class="titreservice upper"><?= the_title()?></a>
                            <p class="descservice"><?= manual_excerpt(get_field("operations"), 0, 250)?></p>
                        </div>
                        <a class="liresuite" href="<?= get_the_permalink()?>">Lire la suite >></a>
                    </div>
                </div>
                <?php endWhile;
                wp_reset_postdata();?>
            <?php else:?>
                <p>Pas de service pour cette catégorie</p>
            <?php endif;?>
            <div class="my-pagination container">
                <?php
                $args_page = array(
                    'base' => @add_query_arg('page','%#%'),
                    'format' => '?page=%#%',
                    'current' => max( 1, get_query_var('page') ),
                    'total' => $loop_service->max_num_pages,
                    'mid_size' => 2,
                    'prev_text' => '<i class="icon-arrow_left"></i>',
                    'next_text' => '<i class="icon-arrow_right"></i>',
                    'type' => 'plain'
                );
                echo paginate_links( $args_page );
                ?>
            </div>
        </div>
        <div class="documents col-lg-6 <?= ($loop_docs->have_posts()) ? '' : 'd-none'?>">
            <h2 class="upper">les <span>documents de référence</span></h2>
            <?php if($loop_docs->have_posts()):?>
            <div class="liste-documents row">
                <?php
                while ( $loop_docs->have_posts() ) : $loop_docs->the_post(); ?>
                <div class="document col-sm-6 col-md-4 col-lg-6 col-xl-4">
                    <div class="blocimg">
                        <div class="image">
                            <img src="<?= the_post_thumbnail_url()?>" alt="document" title="document">
                        </div>
                    </div>
                    <p class="descdoc"><?= get_field("description")?></p>
                    <a href="<?= get_field("document")?>" target="_blank">Télécharger</a>
                </div>
                <?php endwhile;?>
            </div>
            <?php else:?>
            <div class="">
                    <p>Pas de document de référence</p>
            </div>
            <?php endif;?>
        </div>
    </div>

    <div class="contenutrio row">
        <div class="actus col-lg-6 centeredtext">
            <h3 class="upper">les <span>actualités</span></h3>
                <div class="articles row">
                    <?php
                    wp_reset_postdata();
                    while ( $loop->have_posts() ) : $loop->the_post(); ?>
                    <?php $termsArticle = get_the_terms(get_the_ID(), "category");
                    if(get_the_post_thumbnail_url() == ""){

                        $image = asset_url("img/actu_base.jpg");
                    } else {
                        $image = get_the_post_thumbnail_url();
                    }

                    ?>
                    <a class="article col-xl-6 lefttext" href="<?= get_the_permalink()?>" title="Lien vers <?= get_the_title()?>">
                        <div class="cadre">
                            <div class="illustration" style="background-image: url('<?= $image?>')"></div>
                            <p class="datearticle upper white"><?php the_date()?></p>
                        </div>
                        <p class="nomarticle upper"><?php the_title()?></p>
                        <p class="desc"><?= manual_excerpt(get_the_content(), 0, 120)?></p>
                        <p class="lireplus">En lire plus <i class="icon-arrow_right"></i></p>
                        <p class="cat upper white" style="background-color: <?= get_field("couleur",$termsArticle[0])?>"><?= $termsArticle[0]->name?></p>
                    </a>
                    <?php endwhile;?>
                </div>
        </div>
        <div class="contenuright col-lg-6 d-flex flex-column flex-md-row flex-lg-column flex-xl-row centeredtext">
            <div class="juridique col-lg-12 <?= (sizeof($fiches_pratique) > 0) ? 'col-md-6 col-xl-6' : 'col-xl-12 full'?>">
                <h3 class="upper">actualités <span>juridiques</span></h3>
                <?php if($rss_item != null):?>
                <div class="actujuri lefttext">
                    <p class="date upper"><?= date_i18n( "d F Y", strtotime( $rss_item->get_date() ) );?></p>
                    <p class="titre upper"><?= $rss_item->get_title()?></p>
                    <p class="desc"><?= manual_excerpt(wp_strip_all_tags($rss_item->get_content()), 0, 250)?></p>
                    <a target="_blank" href="<?php echo esc_url($rss_item->get_permalink()); ?>">En lire plus</a>
                </div>
                <?php else:?>
                    <p>Pas d'actualité juridique</p>
                <?php endif;?>
            </div>
            <div class="caspratiques <?= (sizeof($fiches_pratique) > 0) ? '' : 'd-none'?> col-md-6 col-lg-12 col-xl-6">
                <h3 class="upper">cas <span>pratiques</span></h3>
                <div class="blocslider">
                    <?php if(sizeof($fiches_pratique) > 0):?>
                    <div class="minislider">
                        <?php foreach ($fiches_pratique as $fiche):?>
                        <div class="slide row white">
                            <a href="<?= get_the_permalink($fiche->ID)?>" class="d-flex align-items-center centeredtext justify-content-center">
                                <p class="upper"><?= $fiche->post_title?></p>
                            </a>
                        </div>
                        <?php endforeach;?>
                    </div>
                    <div class="navminislider">
                        <i class="icon-arrow_left prev"></i>
                        <i class="icon-arrow_right next"></i>
                    </div>
                    <?php else: ?>
                        <p>Pas de fiche pratique pour cette catégorie</p>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/aides_financieres' ); ?>
</section>

<script>

    var $ = jQuery;

    $(function(){

        var link =  $("#ariane").find('a').eq(1).attr("href");
        var tab = link.split('/');
        var cat = tab[tab.length-2];

        $("#ariane").find('a').eq(1).attr("href", '<?= get_the_permalink($options['lien_assistance'])?>#'+cat);

    });
</script>


<?php

get_footer()
    ?>
