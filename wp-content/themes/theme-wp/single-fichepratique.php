<?php

get_header();
$options = get_option('options');

$terms = get_the_terms(get_the_ID(), 'assistancetechnique');

$args_fiches = array(
    'post_type' => 'fichepratique',
    'posts_per_page' => '-1',
    'tax_query' => array(
        array(
            'taxonomy' => 'assistancetechnique',
            'field' => 'slug',
            'terms' => $terms[0]->slug
        )
    ),
);

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}

$fiches_pratique = get_posts($args_fiches);
?>

<section class="fiche-pratique">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_the_permalink($options['lien_assistance'])."#".$terms[0]->slug?>">Retour</a>
    </div>
    <a href="" onclick="javascript:window.print()" class="btnimprimer d-flex flex-column centeredtext align-items-center">
        <i class="icon-print"></i>
        <p>imprimer</p>
    </a>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>

    <div class="topfiche centeredtext" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1><?= the_title()?></h1>
            <?php if(get_field("document", get_the_ID()) != ""):?>
                <a target="_blank" href="<?= get_field("document", get_the_ID())?>" class="btndownload white upper">Télécharger</a>
            <?php endif;?>
        </div>
    </div>
    <div class="contenufiche centeredtext">
        <div class="textefiche">
            <?php the_content()?>
        </div>
        <?php if(get_field("document", get_the_ID()) != ""):?>
            <a target="_blank" href="<?= get_field("document", get_the_ID())?>" class="btndownload white upper">Télécharger</a>
        <?php endif;?>
    </div>
    <?php if(sizeof($fiches_pratique) > 1):?>
    <div class="autresfiches centeredtext">
        <h3 class="upper">autres <span>fiches pratiques</span></h3>
        <div class="slider-fiches white">
            <?php foreach ($fiches_pratique as $fiche):
                if($fiche->ID == get_the_ID())
                {
                    continue;
                }
                ?>
                <div class="slide">
                    <a href="<?= get_the_permalink($fiche->ID)?>" class="d-flex align-items-center">
                        <p class="upper"><?= $fiche->post_title?></p>
                    </a>
                </div>
            <?php endforeach;?>
        </div>
        <div class="navsliderfiches">
            <i class="icon-arrow_left prev"></i>
            <i class="icon-arrow_right next"></i>
        </div>
    </div>
    <?php endif;?>

    <?php get_template_part( 'templates/templates_parts/aides_financieres' ); ?>
</section>

<?php

get_footer();

?>
