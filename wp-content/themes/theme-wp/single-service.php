<?php
get_header();
$options = get_option('options');

$terms = get_the_terms(get_the_ID(), 'assistancetechnique');


function isChild($var)
{
    return !($var->parent == 0);
}

$terms = array_filter($terms, "isChild");
$term_parent = get_term($terms[0]->parent);

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}

?>
<section class="fiche-service">
    <div class="btnretour">

        <?php previous_post_link('%link', ' <i class="icon-full_arrow_left"></i>&nbsp;&nbsp;Précédent', $in_same_term = true, $excluded_terms = '', $taxonomy = 'assistancetechnique'); ?>
    </div>
    <div class="btnsuivant">
        <?php next_post_link('%link', 'Suivant&nbsp;&nbsp;<i class="icon-full_arrow_right"></i>', $in_same_term = true, $excluded_terms = '', $taxonomy = 'assistancetechnique'); ?>
    </div>
    <a href="" onclick="javascript:window.print()" class="btnimprimer d-flex flex-column centeredtext align-items-center">
        <i class="icon-print"></i>
        <p>imprimer</p>
    </a>

    <div class="ariane d-flex flex-wrap align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?> ')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <p class="cat centeredtext upper white" style="background-color: <?= get_field("couleur", $term_parent)?>"><?= $term_parent->name?></p>
            <h1 class="upper"><?= get_the_title()?></h1>
            <div class="paraf">
                <p>Mis à jour le <?= the_modified_date()?></p>
                <?= get_the_content()?>
            </div>
        </div>
    </div>

    <div class="contenufiche-service centeredtext">
        <h2 class="upper">opérations<span> concernées</span></h2>
        <div class="bandeimage d-flex flex-wrap">
            <div class="image" style="background-image: url('<?= get_field("image_operation", get_the_ID())?>') !important"></div>
            <div class="lefttext textebande">
                <?= get_field("operations", get_the_ID())?>
            </div>
        </div>
        <div class="tableau table-responsive lefttext">
            <table>
                <tr class="upper">
                    <td>etape du projet</td>
                    <td>niveau d'intervention ou description des interventions</td>
                    <td class="centeredtext"><?= (get_field("colonne_1", get_the_ID()) != "") ? get_field("colonne_1", get_the_ID()) : "Intervention Gratuite"?></td>
                    <td class="centeredtext"><?= (get_field("colonne_2", get_the_ID()) != "") ? get_field("colonne_2", get_the_ID()) : "Intervention Payante"?></td>
                </tr>
                <?php $tab = get_field("tableau_operations", get_the_ID());

                $infos = array();
                foreach ($tab as $key =>$row):
                    ?>
                <tr class="">
                    <td><?= $row['etape_projet']?></td>
                    <td>
                        <?= apply_filters("content",$row['description'])?>
                    </td>
                    <td class="centeredtext">
                        <?= ($row['intervention_gratuite'])? '<i class="icon-check"></i>' : ''?>
                    </td>
                    <td class="centeredtext">
                        <?= (!$row['intervention_gratuite'] || $row['payant']) ? '<i class="icon-check"></i>' : ''?>
                        <?php
                            if($row['information'] != ""){
                                $infos[] = $key;
                                echo "<small>*".sizeof($infos)."</small>";
                            }?>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>
        </div>
        <div class="text-right bande-infos <?= (sizeof($infos) > 0)? "" : "d-none"?>">
            <?php foreach ($infos as $key => $info):?>
                <p>
                    <small>*<?= $key+1?> <?= get_field("tableau_operations")[$info]["information"]?></small>
                </p>
            <?php endforeach;?>
        </div>
        <div class="text-left bande-infos <?= (get_field("informations_complementaires", get_the_ID()) != "")? "" : "d-none"?>">
            <?= get_field("informations_complementaires", get_the_ID())?>
        </div>
    </div>

    <div class="bande-infos row justify-content-lg-center">
        <?php if(get_field("beneficiaires", get_the_ID()) != ""):?>
            <div class="beneficiaires col-lg-4">
                <h3 class="upper centeredtext">les <span>bénéficiaires</span></h3>
                <?= get_field("beneficiaires", get_the_ID())?>
            </div>
        <?php endif;?>
        <?php if(get_field("partenaire", get_the_ID()) != ""):?>
        <div class="partner col-lg-4 centeredtext">
            <h3 class="upper">notre <span>partenaire</span></h3>
            <a href="<?= (get_field("lien_partenaire", get_the_ID()) !="") ? get_field("lien_partenaire", get_the_ID()) : "" ?>" target="_blank">
                <img src="<?= get_field("partenaire", get_the_ID())?>" style="max-width: 85%; height: auto" alt="Partenaire" title="Partenaire">
            </a>
            <?= apply_filters('the_content', get_field("texte_partenaire", get_the_ID()))?>
        </div>
        <?php endif;?>
        <?php if(get_field("coordonnees", get_the_ID()) != ""):?>
        <div class="coordonnees col-lg-4">
            <h3 class="upper centeredtext">les <span>coordonnées</span></h3>
            <?= get_field("coordonnees", get_the_ID())?>
        </div>
        <?php endif;?>
    </div>

    <?php get_template_part( 'templates/templates_parts/aides_financieres' ); ?>
</section>
<script>

    var $ = jQuery;

    $(function(){

       var link =  $("#ariane").find('a').eq(1).attr("href");
       var tab = link.split('/');
       var cat = tab[tab.length-2];

       $("#ariane").find('a').eq(1).attr("href", '<?= get_the_permalink($options['lien_assistance'])?>#'+cat);

    });
</script>


<?php

get_footer();
?>
