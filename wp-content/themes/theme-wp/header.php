<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width">
    <meta name="format-detection" content="telephone=no"/>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php wp_head(); ?>
    <script>
        var base_url = "<?= site_url()?>";
        var $ = jQuery;

        function setPadding(){
            var h = $(".bloctitre").height();
            $(".serv, .actualites, .fiche-pratique .cms, .contenu-aide, .liste-blocs-assistance, .contenu-article, .contenufiche-service, .contenu-aide-financiere, .contenuduo, .contenucontact ").css("padding-top", h-100);
        }

        document.addEventListener('DOMContentLoaded', function () {

            setPadding();

            window.onresize = function() {
                setPadding();
            }
        });

    </script>
</head>
<body class="<?= get_css(); ?>">
    <?php $options = get_option('options');?>
    <header>
        <div class="logo">
            <a href="<?= site_url()?>"><img src="<?=asset_url()?>img/logo_interne.png" alt="Logo La Drome" title="Logo La Drome"></a>
        </div>
        <div class="menu upper">
            <ul class="d-flex justify-content-end align-items-center">
                <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
                <li class="recherche">
                    <input class="search" type="text" id="recherche" name="s">
                    <i class="loupe icon-search"></i>
                </li>
                </form>
                <li class="connect">
                    <?php if(!is_user_logged_in()):?>
                        <a class="connexion" href="<?= wpmem_login_url() ?>">connexion </a>
                    <?php else:?>
                        <a class="connexion" href="<?= get_the_permalink( get_page_by_title( 'Mon compte' ) )?>">Mon compte </a>
                    <?php endif;?>
                    <i class="icon-connexion"></i>
                </li>
                <li class="hamburger">
                    <a>menu <i class="icon-menu"></i></a>
                </li>
            </ul>

        </div>
        <?php

        $menu = get_nav_menu_items_by_location("primary");

        ?>
        <div class="menu-mobile white">
            <div class="fermer upper hamburger-close d-flex align-items-center">
                fermer
                <i class="icon-close"></i>
            </div>

            <ul class="upper d-flex flex-column justify-content-around">
                <?php
                foreach ($menu as $key => $item):
                    if($item->menu_item_parent == "0"):?>
                        <?php $classe = (sizeof(get_nav_menu_item_children($item->ID, $menu)) > 0) ? "sub" : "" ?>
                        <li class="<?= $classe?>">
                            <a title="<?= $item->attr_title?>" href="<?= $item->url?>"><?= $item->title?></a>
                            <ul class="submenu">
                            <?php
                            foreach ($menu as $subItem):
                                if($subItem->menu_item_parent == $item->ID):?>
                                    <li>
                                        <a title="<?= $subItem->attr_title?>" href="<?= $subItem->url?>"><?= $subItem->title?></a>
                                    </li>
                            <?php
                                endif;
                            endforeach;?>
                            </ul>
                        </li>
                <?php
                    endif;
                endforeach;?>
            </ul>
            <div class="recherchemobile d-flex align-items-center justify-content-center">
                <form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
                    <i class="loupe d-flex icon-search"><p>Recherche</p></i>
                    <input class="search white" type="text" id="recherchemobile" name="s">
                </form>
            </div>
            <div class="connexionmobile">
                <a class="connexion" href="<?= wpmem_login_url() ?>">connexion </a>
                <i class="icon-connexion"></i>
            </div>
        </div>
    </header>