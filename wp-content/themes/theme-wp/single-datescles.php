<?php
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
?>
<style>
    .contenu-article h2:after{
        content: none;
    }
</style>
<section class="page-article">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_post_type_archive_link( 'datescles' );?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <a href="index.php">Accueil</a>
        <span class="arrow">></span>
        <a href="<?= get_post_type_archive_link( 'datescles' );?>">Dates clés</a>
        <span class="arrow">></span>
        <p><?= the_title()?></p>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="bloctitre">
            <p class="cat catentete centeredtext upper white" style="background-color: #00a6e9; max-width: 300px;"><?= date_i18n( "l d F Y", strtotime( get_field("date") ) );?></p>
            <h1 class="upper thinh1"><?= the_title()?></h1>
            <div class="paraf bolder">
                <p class="lieuevent"><i class="icon-where"></i>&nbsp;<b><?php the_field("lieu")?></b></p>
            </div>
        </div>
    </div>
    <div class="contenu-article cms">
        <?php the_content()?>
    </div>
    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php

get_footer();
?>