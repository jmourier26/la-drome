<?php
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url(get_option( 'page_for_posts' )) == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url(get_option( 'page_for_posts' ));
}

$args = [
    'taxonomy'     => 'category',
    'hide_empty'    => false,
    'orderby' => 'id',
    'order'   => 'DESC'
];
$terms = get_terms( $args );

$argsArticle = array(
		        'posts_per_page' => -1,
		        'orderby' => 'date',
		        'order' => "DESC",
	        );
$loop = new WP_Query($argsArticle);
?>

<section class="page-actualites">

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= asset_url("img/default.jpg")?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_the_title(get_option( 'page_for_posts' ))?></h1>
            <p class="paraf">
                <?= get_the_excerpt(get_option( 'page_for_posts' ))?>
            </p>
        </div>
    </div>
    <div class="actus actualites">
        <div class="filtres upper white">
            <ul class="row justify-content-around">
                <?php foreach ($terms as $term):?>
                    <li style="background-color: <?= get_field("couleur", $term)?>" class="cat d-flex justify-content-center align-items-center" data-cat="<?= $term->slug?>"><?= $term->name?></li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="articles row">
            <?php
            while ( $loop->have_posts() ) : $loop->the_post(); ?>
                <?php if(get_the_post_thumbnail_url() == ""){

                $image = asset_url("img/actu_base.jpg");
                } else {
                $image = get_the_post_thumbnail_url();
                }
                ?>

            <?php $termsArticle = get_the_terms(get_the_ID(), "category");?>

            <a href="<?= get_the_permalink()?>" class="article <?= $termsArticle[0]->slug?> col-md-6 col-lg-4 col-xl-3 lefttext">
                <div class="cadre">
                    <div class="illustration one" style="background-image: url('<?= $image?>')"></div>
                    <p class="datearticle upper white"><?= get_the_date()?></p>
                </div>
                <p class="nomarticle upper"><?= the_title()?></p>
                <p class="desc"><?= manual_excerpt(get_the_content(), 0, 120)?> </p>
                <p class="lireplus">En lire plus <i class="icon-arrow_right"></i></p>
                <p class="cat culture upper white" style="background-color: <?= get_field("couleur",$termsArticle[0])?>"><?= $termsArticle[0]->name?></p>
            </a>
            <?php endwhile;?>
        </div>
    </div>


    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php
get_footer();?>


