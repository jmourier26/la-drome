<?php
$options = get_option('options');
?>
<footer class="white">
    <div class="topfooter row justify-content-around">
        <div class="logofooter col-lg-2">
            <img src="<?= asset_url("img/logo-footer.png")?>" alt="Logo La Drome Le département" title="Logo La Drome Le département">
        </div>
        <div class="menubottom col-lg-2 d-flex flex-column">
            <?php $menu1 = get_nav_menu_items_by_location("footer_1"); ?>
            <p class="titrebottom">MENU</p>
            <?php foreach ($menu1 as $item1):?>
                <a title="<?= $item1->attr_title?>" href="<?= $item1->url?>"><?= $item1->title?></a>
            <?php endforeach;?>
        </div>
        <div class="assistancebottom col-lg-2 d-flex flex-column">
            <p class="titrebottom">
                <a href="<?= get_the_permalink($options['lien_assistance'])?>" title="Liens vers notre assistance technique">
                    ASSISTANCE TECHNIQUE
                </a>
            </p>
            <?php $menu2 = get_nav_menu_items_by_location("footer_2"); ?>
            <?php foreach ($menu2 as $item2):?>
                <a title="<?= $item2->attr_title?>" href="<?= $item2->url?>"><?= $item2->title?></a>
            <?php endforeach;?>
        </div>
        <div class="liensutiles col-lg-2 d-flex flex-column">
            <?php $liens = get_posts(array(
                    'post_type' => 'lienutile',
                    'order' => 'ASC',
                    'orderby' => 'date',
                    'posts_per_page' => '5')
                    );?>
            <p class="titrebottom">
                <a href="<?= get_post_type_archive_link("lienutile")?>" title="Voir nos liens utiles">
                    LIENS UTILES
                </a>
            </p>
            <?php foreach ($liens as $lien):?>
                <a title="Lien vers <?= $lien->post_title?>" target="_blank" href="<?= get_field("lien", $lien->ID)?>"><?= $lien->post_title?></a>
            <?php endforeach;?>
            <a title="Lien vers les liens utiles" target="_blank" href="<?= get_post_type_archive_link("lienutile")?>">Voir plus</a>
        </div>
        <div class="contactbottom col-lg-2 d-flex flex-column">
            <p class="titrebottom">
                <a href="<?= get_the_permalink($options['lien_contact'])?>" title="Liens vers notre assistance technique">
                CONTACTEZ-NOUS
                </a>
            </p>
            <p><?= $options['lieu']?><br><?= $options['adresse']?> <br><?= $options['code_postal']?> <?= $options['ville']?></p>
            <div class="row teldiv">
                <i class="icon-tel"></i>
                <p class="telbottom"><?= $options['telephone']?></p>
            </div>
            <div class="socialbottom">
                <a title="Lien vers notre Facebook" href="<?= $options['facebook']?>" target="_blank"><i class="icon-facebook"></i></a>
                <a title="Lien vers notre Twitter" href="<?= $options['twitter']?>" target="_blank"><i class="icon-twitter"></i></a>
            </div>
        </div>
    </div>
    <div class="credits row justify-content-center">
        <?php $now = new DateTime()?>
        <p>Copyright © Département de la Drôme - <?= $now->format("Y")?></p>
        <i class="separateur"></i>
        <p class="l">Tous droits réservés</p>
        <i class="separateur"></i>
        <a title="Mentions légales du site Espace collectivités - La Drôme" href="<?= get_the_permalink($options['lien_mentions'])?>">Mentions Légales</a>
        <i class="separateur"></i>
        <a title="Plan du site Espace collectivités - La Drôme" href="<?= get_the_permalink($options['lien_plan'])?>">Plan du site</a>
        <i class="separateur"></i>
        <a title="Politique de confidentialité du site Espace collectivités - La Drôme" href="<?= get_the_permalink($options['lien_politique'])?>">Politique de confidentialité</a>
    </div>
</footer>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.css" />
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script>
    window.cookieconsent.initialise({
        "palette": {
            "popup": {
                "background": "#252e39"
            },
            "button": {
                "background": "#14a7d0"
            }
        },
        "showLink": true,
        "theme": "edgeless",
        "position": "bottom-right",
        "content": {
            "message": "Notre site est configuré pour permettre l'utilisation de cookies permettant la fourniture de nos services et facilitant la communication par voie électronique. Si vous acceptez l'utilisation de cookies nous vous invitons à cliquer sur « Continuer » et/ou à poursuivre votre navigation sur ce site",
            "dismiss": "Continuer",
            "href": "<?= get_the_permalink($options['lien_politique'])?>",
            "link": "Voir plus",
        }
    });
</script>
<?php wp_footer(); ?>
</body>
</html>