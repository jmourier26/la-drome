<?php

get_header();

$options = get_option("options");

global $query_string;

wp_parse_str( $query_string, $search_query );
$search = new WP_Query( $search_query );
//
//debug($search);die();
?>
<section class="page-actualites">

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= asset_url("img/default.jpg")?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper">Recherche pour : <?= $_GET['s']?></h1>
        </div>
    </div>
    <div class="actus">
        <div class="filtres" style="padding-top: 100px"></div>
        <div class="articles row">
            <?php
            if($search->have_posts()):
                while ( $search->have_posts() ) : $search->the_post(); ?>
                    <?php if(get_the_post_thumbnail_url() == ""){

                        $image = asset_url("img/actu_base.jpg");
                    } else {
                        $image = get_the_post_thumbnail_url();
                    }

                    $pt = get_post_type_object(get_post_type());
                    ?>

                    <?php $termsArticle = get_the_terms(get_the_ID(), "category");?>

                    <a href="<?= get_the_permalink()?>" class="article col-md-6 col-lg-4 col-xl-3 lefttext">
                        <div class="cadre">
                            <div class="illustration one" style="background-image: url('<?= $image?>')"></div>
                        </div>
                        <p class="nomarticle upper"><?= the_title()?></p>
                        <p class="desc"><?= manual_excerpt(get_the_content(), 0, 120)?> </p>
                        <p class="lireplus">En lire plus <i class="icon-arrow_right"></i></p>
                        <p class="cat culture upper white" style="background-color: #00a6e9;"><?= $pt->label?></p>
                    </a>
                <?php endwhile;
                else:
                ?>
                <div class="col-12">
                    <p>Aucun résultat trouvé. Vous pouvez essayer de chercher par mots-clés</p>
                </div>
                <?php endif;?>
        </div>
    </div>


    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>
<?php

get_footer();
    ?>
