var gulp            = require('gulp'),
    sass            = require('gulp-sass'),
    sourcemaps      = require('gulp-sourcemaps'),
    browserSync     = require('browser-sync').create(),
    bower           = require('gulp-bower'),
    notify          = require("gulp-notify"),
    autoprefixer    = require('gulp-autoprefixer'),
    uglify          = require('gulp-uglify'),
    concat          = require('gulp-concat'), 
    rename          = require('gulp-rename'),
    cleanCSS        = require('gulp-clean-css'),
    order           = require("gulp-order"),
    gutil           = require("gulp-util");

var paths = {
    script: ['./src/js/libraries/*.js','./src/js/*.js'],
    sass: './src/scss/**/*.scss',
    php: './*.php',
    css: './assets/css/',
    bower: './bower_components'
};

gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(paths.bower))
});

gulp.task('serve', ['sass', 'js'], function() {
    browserSync.init({
        proxy: "localhost/drome",
        port: 3000
    });

    gulp.watch(paths.sass, ['sass']);
    gulp.watch(paths.script, ['js']);
    // gulp.watch(paths.php).on('change', browserSync.reload);
});

gulp.task('sass', function() {
    gulp.src('./src/scss/bundle.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            outputStyle: 'nested',
            errLogToConsole: true,
            sourcemap: true,
            includePaths: [
                './node_modules/bootstrap-sass/assets/stylesheets',
                './node_modules/susy/sass',
                './src/scss'
            ]
        }).on('error', sass.logError))
        .pipe(sourcemaps.write({includeContent: false, sourceRoot: paths.css}))
        .pipe(gulp.dest(paths.css))
        .pipe(notify({ message: 'Styles task complete', onLast: true }))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    return gulp.src(paths.script)
        .pipe(order([
            "jquery.js",
            "owl.carousel.js",
            "*.js",
            "global.js"
        ]))
        .pipe(concat('bundle.js'))
        //only uglify if gulp is ran with '--type production'
        .pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
        .pipe(gulp.dest('assets/js/'))
        .pipe(browserSync.stream());
});

gulp.task('default', ['serve']);



gulp.task('sass-deploy', function() {
    gulp.src('./src/scss/bundle.scss')
        .pipe(sass({
            outputStyle: 'nested',
            errLogToConsole: true,
            sourcemap: false,
            includePaths: [
                './node_modules/bootstrap-sass/assets/stylesheets',
                './node_modules/susy/sass',
                './src/scss'
            ]
        }).on('error', sass.logError))
        .pipe(cleanCSS())
        .pipe(autoprefixer('last 4 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(gulp.dest(paths.css));
});


gulp.task('deploy', ['sass-deploy', 'js']);