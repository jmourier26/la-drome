<?php
    get_header();
    $options = get_option("options");

    if(get_the_post_thumbnail_url() == ""){

        $image = asset_url("img/default.jpg");
    } else {
        $image = get_the_post_thumbnail_url();
    }
?>
<section class="fiche-pratique">
    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>

    <div class="topfiche centeredtext" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1><?= the_title()?></h1>
        </div>
    </div>
    <div class="centeredtext cms container">
       <?php the_content()?>
    </div>
    <div class="text-center" style="margin-top: 30px">
        <?php if(get_field("fichier", get_the_ID()) != ""):?>
            <a target="_blank" href="<?= get_field("fichier", get_the_ID())?>" class="btndownload white upper"><?= get_field("intitule", get_the_ID())?></a>
        <?php endif;?>
    </div>
    <?php get_template_part( 'templates/templates_parts/aides_financieres' ); ?>
</section>

<?php
get_footer(); 
?>