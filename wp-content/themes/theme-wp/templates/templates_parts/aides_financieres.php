<?php
$options = get_option("options");
?>
<div class="bandeaide d-flex flex-wrap align-items-center justify-content-center white">
    <p>Aide <span>financière</span></p>
    <p class="phrase"><?= $options['text_aides']?></p>
    <a target="_blank" href="<?= get_the_permalink($options['lien_aide'])?>" class="upper link">découvrir <i class="icon-full_arrow_right"></i></a>
</div>