<?php
$options = get_option("options");
?>
<form id="newsletter">
    <div class="newsletter d-flex flex-wrap align-items-center justify-content-center white">
        <p>Inscrivez-vous <br><span>à notre newsletter</span></p>
        <span class="line"></span>
        <div class="containinput">
            <input class="input" id="emailnewsletter" type="email" >
            <label for="email" class="label white">Adresse email</label>
            <div class="row rgpd">
                <div class="col-sm-12">
                    <label for="rgpd">J'accepte les termes et conditions de la <a href="<?= get_the_permalink(3)?>" title="Politique de confidentialité de la Drôme" target="_blank">Politique de Confidentialité</a>.
                        <input type="checkbox" name="rgpd" id="rgpd" value="accept">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <span class="message"></span>
        </div>
        <input type="hidden" id="action" value="subcribeMailPoet">
        <button type="submit" class="upper">inscription <i class="icon-full_arrow_right"></i></button>
    </div>
</form>