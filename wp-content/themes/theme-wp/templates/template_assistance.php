<?php
/**
 * Template Name: Assistance
 */
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}

function isParent($var)
{
    return $var->parent == 0;
}

function isChild($var)
{
    return !($var->parent == 0);
}

$args = [
    'taxonomy'     => 'assistancetechnique',
    'hide_empty'    => false,
    'orderby' => 'id',
    'order'   => 'ASC'
];
$terms = get_terms( $args );

$parent_terms = array_filter($terms, "isParent");
$child_terms = array_filter($terms, "isChild");


?>
<section class="liste-assistance">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_the_title()?></h1>
                <div class="paraf">
                    <?= get_the_content()?>
                </div>
        </div>
    </div>
    <div class="liste-blocs-assistance upper white">
        <div class="filtres">
            <ul class="row justify-content-around">
                <?php foreach ($parent_terms as $pterms):?>
                    <li style="background-color: <?= get_field("couleur", $pterms)?>" class="cat d-flex justify-content-center align-items-center" data-cat="<?= $pterms->slug?>"><?= $pterms->name?></li>
                <?php endforeach;?>
            </ul>
        </div>
        <div class="blocs-assistance row">
            <?php foreach ($child_terms as $cterm):
                $term_parent = get_term($cterm->parent);
                ?>

            <a href="<?=get_term_link($cterm) ?>" class="blocassist <?= $term_parent->slug?> col-md-6 col-lg-4 col-xl-3">
                <div class="bloc d-flex align-items-center justify-content-center flex-column centeredtext" style="background-image: url('<?= get_field('image', $term_parent)?>')">
                    <i style="color:  <?= get_field("couleur", $term_parent)?>" class="<?= get_field('icone', $term_parent)?>"></i>
                    <?= get_field("couleur", $cterm)?>
                    <p class="titrebloc"><?= $cterm->name?></p>
                    <p class="cat" style="background-color: <?= get_field("couleur", $term_parent)?>;"><?= $term_parent->name?></p>
                </div>
            </a>
            <?php endforeach; ?>
        </div>
        <div class="text-center" style="margin-top: 30px">
                <a target="_blank" href="<?= get_field("fichier", get_the_ID())?>" class="btndownload white upper"><?= get_field("intitule", get_the_ID())?></a>
        </div>

    </div>
    <?php get_template_part( 'templates/templates_parts/aides_financieres' ); ?>
</section>


<?php

get_footer()
    ?>
