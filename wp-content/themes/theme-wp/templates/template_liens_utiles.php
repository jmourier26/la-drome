<?php
/**
 * Template Name: Liens utiles
 */
get_header();

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
$options = get_option('options');

$args = array(
    'post_type' => 'lienutile',
    'posts_per_page' => '-1',
);

$loop = new WP_Query($args);
?>
<section class="liens-utiles">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche"  style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_the_title()?></h1>
            <div class="paraf bolder">
                <?= get_the_content()?>
            </div>
        </div>
    </div>
    <div class="liste-liens">
    <?php if($loop->have_posts()):?>
        <?php
        while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="lien">
                <p class="titrelien"><?php the_title()?></p>
                <div class="d-flex flex-column flex-md-row">
                    <div class="imagelien" style="background-image: url('<?= get_the_post_thumbnail_url()?>')"></div>
                    <div class="desclien righttext">
                        <?php the_content()?>
                        <a href="<?= get_field("lien")?>" target="_blank" class="lienplus">En lire plus >></a>
                    </div>
                </div>
            </div>
        <?php endWhile;
        wp_reset_postdata();?>
    <?php else:?>
        <p>Pas encore de lien utile</p>
    <?php endif;?>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php
get_footer();
?>


