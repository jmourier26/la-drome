<?php
/**
 * Template name: Edit user
 */
$options = get_option('options');
if(!is_user_logged_in()){

    wpmem_redirect_to_login();
}

if(current_user_can('editor') || current_user_can('administrator')){
    wp_redirect( site_url()."/wp-admin" );

}

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}


get_header();

$user = wp_get_current_user();

//debug($user->data->ID);
?>

<section class="fiche-pratique">
    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>
    <div class="topfiche centeredtext" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1><?= the_title()?></h1>
            <div class="paraf">
                <?php the_content()?>
            </div>
        </div>
    </div>
    <div class="contenulogin row align-items-center" style="margin-bottom: 50px">
        <div class="col-lg-5 centeredtext">
            <ul class="list-group">
                <a href="<?= get_the_permalink( $options['lien_compte'])?>" class="list-group-item list-group-item-action">
                    Mes alertes
                </a>
                <a href="<?= get_the_permalink()?>" class="list-group-item list-group-item-action active">
                    Mes informations personnelles
                </a>
                <li class="list-group-item list-group-item-action">
                    <?= do_shortcode("[wpmem_logout]")?>
                </li>

            </ul>
        </div>
        <div class="formconnect col-lg-7">
            <?//= do_shortcode("[wpmem_form user_edit]")?>

            <form name="form" method="post" action="<?= get_the_permalink(171)?>" id="wpmem_register_form" class="form centeredtext">
                <div class="erreur_message">

                </div>
                <?php wp_nonce_field( 'wpmem_longform_nonce', '_wpmem_update_nonce', true, true ) ?>
                <div class="identite row justify-content-between">
                    <div class="input col-xl-6 d-flex flex-column">
                        <label>Nom*</label>
                        <input name="first_name" type="text" id="first_name" value="<?= get_user_meta($user->data->ID, "last_name", true) ?>" class="textbox" required="">
                    </div>
                    <div class="input col-xl-6 d-flex flex-column">
                        <label for="last_name">Prénom*</label>
                        <input name="last_name" type="text" id="last_name" value="<?= get_user_meta($user->data->ID, "first_name", true) ?>" class="textbox" required="">
                    </div>
                </div>
                <div class="coordonnees row justify-content-between">
                    <div class="input col-xl-12 d-flex flex-column">
                        <label for="user_email">Email*</label>
                        <input name="user_email" type="email" id="user_email" value="<?= $user->data->user_email ?>" class="textbox" required="">
                    </div>
                </div>
                <div class="structure row justify-content-between">
                    <div class="input col-xl-6 d-flex flex-column">
                        <label for="structure">Structure</label>
                        <input name="structure" type="text" id="structure" value="<?= get_user_meta($user->data->ID, "structure", true) ?>" class="textbox" >
                    </div>
                    <div class="input col-xl-6 d-flex flex-column">
                        <label for="fonction">Fonction</label>
                        <input name="fonction" type="text" id="fonction" value="<?= get_user_meta($user->data->ID, "fonction", true) ?>" class="textbox">
                    </div>
                </div>
                <div class="check d-flex">
                    <input type="checkbox" value="agree" name="tos" id="tos">
                    <label for="tos" class="checkmark"></label>
                    <p>J'accepte les termes et conditions de la politique de confidentialité. Lisez les Mentions légales du site.</p>
                </div>
                <input name="a" type="hidden" value="update">
                <input name="wpmem_reg_page" type="hidden" value="<?= get_the_permalink(get_the_ID())?>">
                <input class="btnvalider centre white centeredtext" name="submit" type="submit" value="Modifier mes informations">
            </form>
            <a href="<?= get_the_permalink( $options['lien_mdp'])?>">Modifier mon mot de passe</a>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php

get_footer();

?>
