<?php
/**
 *  Template name: mot de passe
 */
$options = get_option('options');
get_header();

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
?>

<style>

    label[for="user"] {
        display: none;
    }


</style>

<section class="fiche-pratique">
    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_the_permalink( $options['lien_compte'])?>">Retour</a>
    </div>
    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1><?= the_title()?></h1>
            <div class="paraf">
                <?php the_content()?>
            </div>
        </div>
    </div>
    <div class="contenufiche centeredtext">
        <div class="textefiche">
            <?= do_shortcode("[wpmem_form password]")?>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<script>
    $(function(){

        $("#user").attr("type", "hidden");

        $("#wpmem_pwdreset_form").submit(function(e){

            e.preventDefault();


            var email = $("#email").val();
            console.log(email);

            $("#user").val(email);

            $('input[type="submit"]').removeAttr("name");

            $("#wpmem_pwdreset_form").unbind("submit").submit();
        });
    });
</script>

<?php

get_footer();

?>
