<?php
/**
 * Template Name: Compte custom
 */

$options = get_option("options");

if(!is_user_logged_in()){

    wpmem_redirect_to_login();
}

if(current_user_can('editor') || current_user_can('administrator')){
    wp_redirect( site_url()."/wp-admin" );

}

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}

$args = [
    'taxonomy'     => 'category',
    'hide_empty'    => false,
    'orderby' => 'id',
    'order'   => 'DESC'
];
$terms = get_terms( $args );

if(isset($_POST['submit']))
{
    setSavedCategories($_POST, get_current_user_id());
}

//Récupération des catégories enregistrées par le current user
$categories = getSavedCategories(get_current_user_id());

get_header();
?>

<section class="fiche-pratique">
    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div id="ariane">','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1><?= the_title()?></h1>
            <div class="paraf">
                <?php the_content()?>
            </div>
        </div>
    </div>
    <div class="contenulogin row align-items-center" style="margin-bottom: 50px">
        <div class="col-lg-5 centeredtext">
            <ul class="list-group">
                <a href="<?= get_the_permalink( $options['lien_compte'])?>" class="list-group-item list-group-item-action active">
                    Mes alertes
                </a>
                <a href="<?= get_the_permalink( $options['lien_infos'])?>" class="list-group-item list-group-item-action">
                    Mes informations personnelles
                </a>
                </a>
                <li class="list-group-item list-group-item-action">
                    <?= do_shortcode("[wpmem_logout]")?>
                </li>
            </ul>
        </div>
        <div class="formconnect col-lg-7">
            <div class="multicheck">
                <h6 class="grey lefttext">Gérer vos alertes</h6>
                <div class="d-flex flex-wrap">
                    <form action="<?= get_the_permalink(get_the_ID())?>" method="post" class="centeredtext">
                        <?php foreach ($terms as $term):?>
                            <div class="check d-flex">
                                <input id="<?= $term->slug?>" type="checkbox" name="categories[]" <?= in_array($term->term_id, $categories)? "checked" : "" ?>  value="<?= $term->term_id?>">
                                <label style="background-color: <?= get_field("couleur", $term)?>" for="<?= $term->slug?>" class="checkmark alertes"></label>
                                <p><?= $term->name?></p>
                            </div>
                        <?php endforeach;?>
                        <div class="col-12" style="margin-top: 30px">
                            <input class="btnvalider centre white centeredtext" name="submit" type="submit" value="Valider">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php
get_footer();
?>
