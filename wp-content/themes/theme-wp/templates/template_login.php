<?php
/**
 * Template Name: Login custom
 */
$options = get_option('options');

if(is_user_logged_in()){

    if(isset($_GET['redirect_to']))
    {
        wp_redirect($_GET['redirect_to']);
    }

    if(current_user_can('editor') || current_user_can('administrator')){
        wp_redirect( site_url()."/wp-admin" );

    } else {

        wp_redirect(get_the_permalink($options['lien_compte']) );

    }

}
get_header();

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}


?>

<section class="connexion">
    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?php the_title()?></h1>
        </div>
    </div>

    <div class="contenulogin row">
        <div class="illustration col-lg-5 centeredtext" style="background-image: url('<?= get_field("illustration")?>')"></div>
        <div class="formconnect col-lg-7">
            <div class="bandeliens row justify-content-around align-items-center">
                <p class="blue">Vous avez déjà un compte ?</p>
                <p class="grey line">|</p>
                <a class="grey link" href="<?= get_the_permalink($options['lien_creation_compte'])?>">Créer un compte</a>
            </div>
            <div id="wpmem_login"><a id="login"></a>
                <form action="<?= get_the_permalink()?>" method="POST" id="wpmem_login_form" class="form centeredtext">
                    <div class="row justify-content-center">
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="log">Email*</label>
                            <input name="log" type="text" id="log" value="<?= (isset($_POST['log'])) ? $_POST['log'] : ''?>" class="username" />
                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="pwd">Mot de passe*</label>
                            <input name="pwd" type="password" id="pwd" class="password" />
                        </div>
                    </div>
                    <?php
                        $redirect = get_the_permalink(get_the_permalink($options['lien_creation_compte']));
                    ?>
                    <input name="redirect_to" type="hidden" value="<?= (isset($_GET['redirect_to'])) ? $_GET['redirect_to'] : $redirect?>" />
                    <input name="a" type="hidden" value="login" />
                    <div class="row justify-content-center">
                        <div class="input col-xl-6 text-left">
                            <label for="rememberme">
                                <input name="rememberme" type="checkbox" id="rememberme" value="forever" />&nbsp; &nbsp;Se souvenir de moi
                            </label>
                        </div>
                    </div>

                    <input type="submit" name="Submit" value="Se connecter" class="btnvalider centre white centeredtext" />
                    <a class="oublie centeredtext" href="<?= get_the_permalink($options['lien_mdp'])?>">Mot de passe oublié ?</a>
                </form>
            </div>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>


<?php

get_footer();

?>



