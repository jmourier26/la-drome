<?php
/**
 * Template Name: Aides Financière
 */
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
?>

<section class="aide">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= the_title()?></h1>
            <div class="paraf">
                <?= the_content()?>
            </div>
        </div>
    </div>
    <div class="contenu-aide row justify-content-between">
        <a href="<?= get_field("lien_bloc_1")?>" class="col-xl-6">
            <div class="bloc centeredtext d-flex justify-content-center align-items-center" style="background-image: url('<?= get_field("image_bloc_1")?>')">
                <h2 class="upper white"><?= get_field("titre_bloc_1")?></h2>
            </div>
        </a>
        <a target="_blank" href="<?= get_field("lien_bloc_2")?>" class="col-xl-6" >
            <div class="bloc centeredtext d-flex justify-content-center align-items-center" style="background-image: url('<?= get_field("image_bloc_2")?>')">
                <h3 class="upper white"><?= get_field("titre_bloc_2")?></h3>
            </div>
        </a>
    </div>
</section>

<?php

get_footer();

    ?>
