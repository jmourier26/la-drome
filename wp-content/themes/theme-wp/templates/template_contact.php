<?php
/**
 * Template Name: Contact
 */
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
?>

<section class="contact">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= site_url()?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <?php
        if ( function_exists('yoast_breadcrumb') ) {
            yoast_breadcrumb( '<div>','</div>' );
        }
        ?>
    </div>

    <div class="topfiche"  style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?= get_the_title()?></h1>
            <div class="paraf bolder">
                <?= get_the_content()?>
            </div>
        </div>
    </div>

    <div class="contenucontact row">
        <div class="infoscontact col-lg-4 centeredtext">
            <h2 class="centeredtext">Besoin de nous contacter ?</h2>
            <img class="marge" src="<?= asset_url("img/logo.jpg")?>" alt="Logo La Drôme" title="Logo La Drôme">
            <div class="lefttext marge">
                <div class="d-flex">
                    <i class="icon-pin grey"></i>
                    <div class="margebot">
                        <p><?= $options['lieu']?></p>
                        <p><?= $options['adresse']?></p>
                        <p><?= $options['code_postal']?> <?= $options['ville']?></p>
                    </div>
                </div>
                <div class="d-flex">
                    <i class="icon-tel_contact grey"></i>
                    <p>Téléphone <?= $options['telephone']?></p>
                </div>
            </div>
            <div class="d-flex justify-content-center marge">
                <a title="Lien vers notre Facebook" href="<?= $options['facebook']?>" target="_blank"><i class="icon-facebook blue"></i></a>
                <a title="Lien vers notre Twitter" href="<?= $options['twitter']?>" target="_blank"><i class="icon-twitter blue"></i></a>
            </div>
        </div>
        <div class="formulairecontact col-lg-8">
            <?php
            echo do_shortcode(
                '[contact-form-7 title="Contact"]'
              );
            ?>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php
get_footer()
?>
