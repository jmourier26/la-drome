<?php
/**
 * Template Name: Inscription custom
 */
$options = get_option('options');
if(is_user_logged_in()){

    if(current_user_can('editor') || current_user_can('administrator')){
        wp_redirect( site_url()."/wp-admin" );

    } else {
        
        wp_redirect( get_the_permalink($options['lien_compte']));
    }

}

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}
get_header();
?>

<section class="connexion" id="inscription-custom">
    <div class="ariane row align-items-center">
        <a href="index.php">Accueil</a>
        <span class="arrow">></span>
        <p>Connexion</p>
    </div>

    <div class="topfiche" style="background-image: url('<?= $image?>')">
        <div class="image"></div>
        <div class="bloctitre">
            <h1 class="upper"><?php the_title()?></h1>
        </div>
    </div>

    <div class="contenulogin row">
        <div class="illustration col-lg-5 centeredtext" style="background-image: url('<?= get_field("illustration")?>')"></div>
        <div class="formconnect col-lg-7">
            <div class="bandeliens row justify-content-around align-items-center">
                <a href="<?= wpmem_login_url() ?>" class="grey link">Vous avez déjà un compte ?</a>
                <p class="grey line">|</p>
                <p class="blue">Créer un compte</p>
            </div>
            <div id="wpmem_reg"><a name="register"></a>
                <div class="d-none">
                    <?php
                    echo do_shortcode("[wpmem_form register]");
                    ?>
                </div>
                <form name="form" method="post" action="<?= get_the_permalink($options['lien_inscription'])?>" id="wpmem_register_form-1" class="form centeredtext">
                    <div class="erreur_message">

                    </div>
                    <?php wp_nonce_field( 'wpmem_longform_nonce', '_wpmem_register_nonce', true, true ) ?>
                    <div class="identite row justify-content-between">
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="last_name">Prénom*</label>
                            <input name="last_name" type="text" id="last_name" value="<?= isset($_POST['last_name'])? $_POST['last_name'] : "" ?>" class="textbox" required="">
                        </div>
                        <div class="input col-xl-6 d-flex flex-column">
                            <label>Nom*</label>
                            <input name="first_name" type="text" id="first_name" value="<?= isset($_POST['first_name'])? $_POST['first_name'] : "" ?>" class="textbox" required="">
                        </div>
                    </div>
                    <div class="coordonnees row justify-content-between">
                        <div class="input col-xl-12 d-flex flex-column">
                            <label for="user_email">Email*</label>
                            <input name="user_email" type="email" id="user_email-1" value="<?= isset($_POST['user_email'])? $_POST['user_email'] : "" ?>" class="textbox" required="">
                        </div>
                    </div>
                    <div class="coordonnees row justify-content-between">
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="password">Mot de passe*</label>
                            <input name="password" type="password" id="password" value="" class="textbox" required="">
                        </div>
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="confirm_password">Confirmation du mot de passe*</label>
                            <input name="confirm_password" type="password" id="password" value="" class="textbox" required="">
                        </div>
                    </div>
                    <div class="structure row justify-content-between">
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="structure">Structure</label>
                            <input name="structure" type="text" id="structure" value="<?= isset($_POST['structure'])? $_POST['structure'] : "" ?>" class="textbox" >
                        </div>
                        <div class="input col-xl-6 d-flex flex-column">
                            <label for="fonction">Fonction</label>
                            <input name="fonction" type="text" id="fonction" value="<?= isset($_POST['fonction'])? $_POST['fonction'] : "" ?>" class="textbox">
                        </div>
                    </div>
                    <div class="check d-flex">
                        <input type="checkbox" value="agree" name="tos" id="tos">
                        <label for="tos" class="checkmark"></label>
                        <p>J'accepte les termes et conditions de la politique de confidentialité. <a target="_blank" href="<?= get_the_permalink($options['lien_politique'])?>">Lire notre politique de confidentialité.</a></p>
                    </div>
                    <div class="multicheck">
                        <p class="grey lefttext">Gérer vos alertes</p>
                        <?php
                        $args = [
                            'taxonomy'     => 'category',
                            'hide_empty'    => false,
                            'orderby' => 'id',
                            'order'   => 'DESC'
                        ];
                        $terms = get_terms( $args );
                        ?>
                        <div class="d-flex flex-wrap">
                            <?php foreach ($terms as $term):?>
                            <div class="check d-flex">
                                <input type="checkbox" name="categories[]" <?= (isset($_POST['categories']) && in_array($term->term_id, $_POST['categories']))? "checked" : "" ?> value="<?= $term->term_id?>">
                                <label style="background-color: <?= get_field("couleur", $term)?>" class="checkmark alertes"></label>
                                <p><?= $term->name?></p>
                            </div>
                            <?php endforeach;?>
                        </div>
                    </div>
                        <input name="a" type="hidden" value="register">
                        <input name="wpmem_reg_page" type="hidden" value="<?= get_the_permalink($options['lien_compte'])?>">
                        <input name="username" type="hidden" id="username-1" value="<?= isset($_POST['username'])? $_POST['username'] : "" ?>" class="textbox">
                        <input class="btnvalider centre white centeredtext" type="submit" id="envoyer" value="Créer un compte">
                </form>
            </div>
        </div>
    </div>

    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>

<?php

get_footer();

?>
