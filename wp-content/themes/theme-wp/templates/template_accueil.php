<?php
    /**
     * Template Name: Accueil
     */
    get_header("home");
    $options = get_option('options');
?>

    <section class="home">
        <div class="entete d-flex flex-column justify-content-center" style="background-image: url('<?= get_the_post_thumbnail_url()?>')">
            <h1 class="upper white" data-aos="fade-right"
                data-aos-duration="1800"><?= get_bloginfo("name") ?></h1>
            <p class="white" data-aos="fade-right"
               data-aos-duration="1800"><?= get_bloginfo("description") ?></p>
        </div>
        <div class="containerrow">
            <div class="bandeau row">
                <div class="actus col-lg-7 centeredtext" style="padding: 0">
                    <h2 class="upper home">dernières <span>actualités</span></h2>
                    <a class="voirtout" href="<?= get_post_type_archive_link( 'post' );?>">Voir toutes les actualités</a>
                    <?php
                        $argsArticle = array(
                            'posts_per_page' => 2,
                            'orderby' => 'date',
                            'order' => "DESC",
                        );
                        $loop = new WP_Query($argsArticle);
                    ?>
                    <div class="articles row">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post(); ?>
                            <?php $termsArticle = get_the_terms(get_the_ID(), "category");
                            if(get_the_post_thumbnail_url() == ""){

                                $image = asset_url("img/actu_base.jpg");
                            } else {
                                $image = get_the_post_thumbnail_url();
                            }

                            ?>
                            <a href="<?= get_the_permalink()?>" class="article col-xl-6 lefttext">
                                <div class="cadre">
                                    <div class="illustration one" style="background-image: url('<?= $image?>')"></div>
                                    <p class="datearticle upper white"><?= get_the_date()?></p>
                                </div>
                                <p class="nomarticle upper"><?= the_title()?></p>
                                <p class="desc"><?= manual_excerpt(get_the_content(), 0, 120)?></p>
                                <p class="lireplus">En lire plus <i class="icon-arrow_right"></i></p>
                                <p class="cat cat-clt upper white"  style="background-color: <?= get_field("couleur",$termsArticle[0])?>"><?= $termsArticle[0]->name?></p>
                            </a>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
                <div class="datescles col-lg-5 centeredtext">
                    <?php
                    $argsAgenda = array(
                        'post_type' => 'datescles',
                        'posts_per_page' => 2,
                        'orderby' => 'meta_value',
                        'meta_key'=> 'date',
                        'order' => "ASC",
                        'meta_query' => array(
                            array(
                                'key'		=> 'date',
                                'compare'	=> '>=',
                                'value'		=> $today,
                            )
                        ),
                    );
                    $loopAgenda = new WP_Query($argsAgenda);
                    ?>
                    <h2 class="upper home">les <span>dates clés</span></h2>
                    <a class="voirtout" href="<?= get_post_type_archive_link( 'datescles' );?>">Voir toutes les dates clés</a>
                    <div class="dates row">
                        <?php
                        while ( $loopAgenda->have_posts() ) : $loopAgenda->the_post(); ?>
                            <?php
                            if(get_the_post_thumbnail_url() == ""){

                                $image = asset_url("img/actu_base.jpg");
                            } else {
                                $image = get_the_post_thumbnail_url();
                            }
                            ?>
                        <div class="evenement col-xl-5 lefttext">
                            <div class="illustration"  style="background-image: url('<?= $image?>')"></div>
                            <p class="nomevent upper"><?php the_title()?></p>
                            <p class="lieuevent"><i class="icon-where"></i><?php the_field("lieu")?></p>
                            <p class="desc"><?= manual_excerpt(get_the_content(), 0, 100)?></p>
                            <a href="<?= get_the_permalink()?>">En lire plus <i class="icon-arrow_right"></i></a>
                            <p class="dateevent upper white"><?= date_i18n( "d F Y", strtotime( get_field("date") ) );?></p>
                        </div>
                        <?php endwhile;
                        wp_reset_postdata();
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="assistance centeredtext">
            <?php
            $args = [
                'taxonomy'     => 'assistancetechnique',
                'parent'        => 0,
                'hide_empty'    => false,
                'orderby' => 'id',
                'order'   => 'ASC'
            ];
            $terms = get_terms( $args );

            ?>
            <?php if(sizeof($terms) > 2):?>
                <h3 class="upper home">Assistance <span>technique</span></h3>
                <div class="containerrow">
                    <div class="lignesup row justify-content-between white" data-aos="fade-up"
                         data-aos-duration="1000">
                        <div style="background-image: url('<?= get_field('image', $terms[0])?>');" class="amenagement bloc up col-lg-7">
                            <i style="color: <?= get_field('couleur', $terms[0])?>" class="<?= get_field('icone', $terms[0])?>"></i>
                            <h3><?= $terms[0]->name?></h3>
                            <p class="textehover">
                                <?= manual_excerpt($terms[0]->description, 0, 100)?>
                            </p>
                            <a href="<?= get_the_permalink($options['lien_assistance'])."#".$terms[0]->slug?>" class="btnhover upper">Découvrir</a>
                        </div>

                        <div style="background-image: url('<?= get_field('image', $terms[1])?>'); --color: <?= get_field('couleur', $terms[1])?>;" class="tourisme bloc up col-lg-4">
                            <i style="color: <?= get_field('couleur', $terms[1])?>" class="<?= get_field('icone', $terms[1])?>"></i>
                            <h3><?= $terms[1]->name?></h3>
                            <p class="textehover">
                                <?= manual_excerpt($terms[1]->description, 0, 100)?>
                            </p>
                            <a href="<?= get_the_permalink($options['lien_assistance'])."#".$terms[1]->slug?>" class="btnhover upper">Découvrir</a>
                        </div>
                    </div>
                </div>
                <div class="containerrow">
                    <div class="ligneinf row justify-content-between white" data-aos="fade-down"
                         data-aos-duration="1000">
                        <?php for($i=2 ; $i < sizeof($terms) ;$i++):?>
                        <div style="background-image: url('<?= get_field('image', $terms[$i])?>'); --color: <?= get_field('couleur', $terms[$i])?>;" class="bloc bloc_4 down col-xl-3">
                            <i style="color: <?= get_field('couleur', $terms[$i])?>" class="<?= get_field('icone', $terms[$i])?>"></i>
                            <p class="categoriebloc"><?= $terms[$i]->name?></p>
                            <p class="textehover">
                                <?= manual_excerpt($terms[$i]->description, 0, 100)?>
                            </p>
                            <a href="<?= get_the_permalink($options['lien_assistance'])."#".$terms[$i]->slug?>" class="btnhover upper">Découvrir</a>
                        </div>
                        <?php endfor;?>
                    </div>
                </div>
            <?php endif;?>
        </div>
        <div class="containerrow">
            <div class="bandeaublanc row" style="padding: auto 0">
                <div class="aide col-lg-6 centeredtext" data-aos="fade-left"
                     data-aos-duration="1000">
                    <p class="title home upper"><?= get_field("titre_bloc_1")?></p>
                    <div class="blocaide row">
                        <div class="illustr col-md-6" style="background-image: url('<?= get_field("image_bloc_1")?>')"></div>
                        <div class="paraf col-md-6">
                            <p class="lefttext"><?= get_field("texte_bloc_1")?></p>
                            <a class="upper" href="<?= get_field("lien_bloc_1")?>">découvrir</a>
                        </div>
                    </div>
                </div>
                <div class="demat col-lg-6 centeredtext" data-aos="fade-right"
                     data-aos-duration="1000">
                    <p class="title home upper"><?= get_field("titre_bloc_2")?></p>
                    <div class="blocdemat row">
                        <div class="illustr col-md-6" style="background-image: url('<?= get_field("image_bloc_2")?>')"></div>
                        <div class="paraf col-md-6">
                            <p class="lefttext"><?= get_field("texte_bloc_2")?></p>
                            <a target="_blank" class="upper" href="<?= get_field("lien_bloc_2")?>">découvrir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
    </section>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tilt.js/1.2.1/tilt.jquery.min.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script src="<?= asset_url("js/js-cookie.js")?>"></script>
    <script>
        var is_touch_device = function(){
            try{
                document.createEvent("TouchEvent");
                return true;
            } catch(e){
                return false;
            }
        }

        document.addEventListener('DOMContentLoaded', function () {


            if(Cookies.get("anim_entree") === "1"){
                $("[data-aos]").removeAttr("data-aos");
            } else {
                AOS.init();
                Cookies.set("anim_entree", "1");
            }


            if(!is_touch_device())
            {
                $('.bloc ').tilt({
                    scale: 1,
                    glare: true,
                    maxTilt: 5,
                });
            }


        });
    </script>
<?php
    get_footer(); 
?>