<?php
get_header();
$options = get_option('options');

if(get_the_post_thumbnail_url() == ""){

    $image = asset_url("img/default.jpg");
} else {
    $image = get_the_post_thumbnail_url();
}

$terms = get_the_terms(get_the_ID(), 'category');
?>
<style>
    .contenu-article h2:after{
        content: none;
    }
</style>
<section class="page-article">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_post_type_archive_link( 'post' );?>">Retour</a>
    </div>

    <div class="ariane row align-items-center">
        <a href="<?= site_url()?>">Accueil</a>
        <span class="arrow">></span>
        <a href="<?= get_post_type_archive_link( 'post' );?>">Actualités</a>
        <span class="arrow">></span>
        <p><?= the_title()?></p>
    </div>

    <div class="topfiche article" style="background-image: url('<?= $image?>')">
        <div class="bloctitre">
            <p class="cat catentete centeredtext upper white" style="background-color: <?= get_field("couleur", $terms[0])?>"><?= $terms[0]->name?></p>
            <h1 class="upper thinh1"><?= the_title()?></h1>
            <div class="paraf bolder">
                <?= get_field("introduction")?>
            </div>
        </div>
    </div>
    <div class="contenu-article cms">
        <?php the_content()?>
    </div>

    <div class="commentaires centeredtext">
        <h3 class="upper centeredtext">les <span>commentaires</span></h3>
        <?php comments_template(); ?>
    </div>

    <div class="morearticles">
        <h3 class="upper centeredtext">cela pourrait <span>vous intéresser</span></h3>
        <?php
        $args = array(
            'post__not_in' => array(get_the_ID()),
            'post_type' => 'post',
            'orderby'   => 'rand',
            'posts_per_page' => 4,

        );

        $the_query = new WP_Query( $args );
        ?>
        <div class="actus">
            <div class="articles row">
                <?php
                while ( $the_query->have_posts() ) : $the_query->the_post();
                    if(get_the_post_thumbnail_url() == ""){

                        $image = asset_url("img/actu_base.jpg");
                    } else {
                        $image = get_the_post_thumbnail_url();
                    }

                    $termsArticle = get_the_terms(get_the_ID(), "category");
                    ?>
                <a href="<?= get_the_permalink()?>" class="article catclt col-md-6 col-lg-4 col-xl-3 lefttext">
                    <div class="cadre">
                        <div class="illustration" style="background-image: url('<?= $image?>')"></div>
                        <p class="datearticle upper white"><?= get_the_date()?></p>
                    </div>
                    <p class="nomarticle upper"><?= the_title()?></p>
                    <p class="desc"><?= manual_excerpt(get_the_content(), 0, 120)?></p>
                    <p class="lireplus">En lire plus <i class="icon-arrow_right"></i></p>
                    <p class="cat culture upper white" style="background-color: <?= get_field("couleur",$termsArticle[0])?>"><?= $termsArticle[0]->name?></p>
                </a>
                <?php endwhile;?>
            </div>
        </div>
    </div>


    <?php get_template_part( 'templates/templates_parts/newsletter' ); ?>
</section>


<?php

get_footer();

?>
