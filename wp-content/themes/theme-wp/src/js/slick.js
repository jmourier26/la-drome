var $ = jQuery;

jQuery(document).ready(function(){

    ///slide changeante en responsive
    $('.slider-fiches').slick({
        dots: false,
        prevArrow: $('.navsliderfiches .prev'),
        nextArrow: $('.navsliderfiches .next'),
        slidesToShow: 5,
        slidesToScroll: 5,
        infinite: false,
        speed: 2000,
        responsive: [
            {
              breakpoint: 1100,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });

    //slide en autoplay
    $('.minislider').slick({
        dots: false,
        prevArrow: $('.navminislider .prev'),
        nextArrow: $('.navminislider .next'),
        slidesToShow: 1,
        slidesToScroll: 1,
        swipeToSlide: true,
        infinite: true,
        autoplay: false,
        autoplaySpeed: 2000,
    });

});