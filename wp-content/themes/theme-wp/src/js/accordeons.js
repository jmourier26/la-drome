
jQuery(document).ready(function(){

    var acc = document.getElementsByClassName("accordeon");
    var i;

    for (i = 0; i < acc.length; i++) {
        acc[i].onclick = function(){

            this.classList.toggle("active");
            this.classList.toggle("hide-print");
            this.nextElementSibling.classList.toggle("show");
            this.nextElementSibling.classList.toggle("hide-print");
        }
    }

});