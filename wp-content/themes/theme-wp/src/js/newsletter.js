var $ = jQuery;
var ajax_url = directory_uri.ajax_url;

if($("#newsletter").length > 0){

    $(function(){

        if($("#emailnewsletter").val() !== "")
        {
            $("#newsletter .label").addClass("active");
        }


        $("#emailnewsletter").focus(function(){
            $("#newsletter .label").addClass("active");
        });

        $("#emailnewsletter").focusout(function(){
            if($(this).val() === '')
            {
                $("#newsletter .label").removeClass("active");
            }
        });



        $("#newsletter").on("submit", function(e){

            e.preventDefault();

            var email = $("#emailnewsletter").val();
            var action = $("#action").val();

            if(email === "")
            {
                $("#newsletter span.message").addClass("text-danger").text("Une adresse email valide est obligatoire")
                return;
            }

            if(!$("#rgpd").is(":checked")){

                $("#newsletter span.message").addClass("text-danger").text("Veuillez accepter notre politique de confidentialité")
                return;
            }

            $.ajax({
                type:"POST",
                url: ajax_url,
                dataType: "json",
                data:{
                    'action': action,
                    'email' : email},
                error: function (lignes) {
                    $("#newsletter span.message").addClass("text-danger").text("Imposible de joindre le serveur")
                    console.log("Imposible de joindre le serveur");
                },
                success : function(response){

                    console.log(response);
                    if(response["erreur"] !== null)
                    {
                        $("#newsletter span.message").addClass("text-danger").text(response["erreur"])
                    } else {
                        console.log(response);
                        var success = "Un email de validation a été envoyé à l'adresse "+email+".<br>"+
                            "Votre inscription sera prise en compte une fois l'abonnement confirmé";
                        $("#newsletter span.message").removeClass("text-danger").addClass("text-primary").html(success)

                        $("#emailnewsletter").val("");
                        $('#rgpd').attr('checked', false);
                    }

                }

            })

        })
    })

}