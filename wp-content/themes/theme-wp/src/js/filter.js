var $ = jQuery;
var $cat = "";

jQuery(document).ready(function(){


    var url =  document.location.href, idx = url.indexOf("#")
    var hash = idx != -1 ? url.substring(idx+1) : "";

    if(hash !== "")
    {
        trieAssistance(hash);
    }

    $('.filtres ul li').click(function() {

        trieAssistance($(this).attr('data-cat'));
        return false;

    });



    $('.filtres.actus ul li').click(function() {

        trieActu($(this).attr('data-cat'));
        return false;

    });
});



function trieActu(categorie){

    var articles = $('.articles');

    if (categorie === 'catsol') {
        $('.article', articles).removeClass('blocnone');
    } else {
        $('.article', articles).each(function() {
            if ($(this).hasClass(categorie)) {
                $(this).removeClass('blocnone');
            } else {
                $(this).addClass('blocnone');
            }
        });
    }
}

function trieAssistance(cat)
{
    var liste = $('.blocs-assistance');

    if ($cat === cat) {
        $('.blocassist', liste).removeClass('blocnone');
        $cat = "";
    } else {
        $cat = cat;
        $('.blocassist', liste).each(function() {
            if ($(this).hasClass(cat)) {
                $(this).removeClass('blocnone');
            } else {
                $(this).addClass('blocnone');
            }
        });
    }
}





