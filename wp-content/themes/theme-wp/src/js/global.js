var $ = jQuery;
var ajax_url = directory_uri.ajax_url;

if($("#inscription-custom").length > 0){




    $(function(){

        var messageErreurs = $(".wpmem_msg").clone();

        $(".erreur_message").append(messageErreurs);

        $("#wpmem_register_form-1").submit(function(e){

            e.preventDefault();


            var email = $("#user_email-1").val();

            $("#username-1").val(email);

            //document.getElementById("wpmem_register_form-1").subm
            $(this).unbind("submit").submit();
        });
    });
}


if($(".commentaires").length > 0){

    $(function() {
        $(".openComment").on("click", function(e){
            e.preventDefault();

            $("#commentform").show();
            $(".openComment").hide();
        });


        // tinymce.init({
        //     selector: 'textarea.texteditor',
        //     height: 380,
        //     language : 'fr_FR',
        //     entity_encoding : "raw",
        //     force_br_newlines : true,
        //     force_p_newlines : false,
        //     forced_root_block : '',
        //     plugins: [
        //         'advlist autolink lists link image charmap print preview anchor',
        //         'searchreplace visualblocks code fullscreen',
        //         'insertdatetime media table paste code',
        //         'imagetools'
        //     ],
        //     toolbar: 'insertfile undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor',
        //     // content_css: [
        //     //     base_url+'assets/plugins/tiny_mce/tinymce.css',
        //     //     base_url+'assets/dist/css/tiny.css',
        //     // ],
        //     menu: {
        //         insert: {title: 'Insert', items: 'link image media | charmap anchor'},
        //         format: {title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat'},
        //         table: {title: 'Table', items: 'inserttable tableprops deletetable | cell row column'},
        //         tools: {title: 'Tools', items: 'spellchecker code'}
        //     },
        //     image_advtab: true,
        //     relative_urls:false,
        //     images_upload_handler: function (blobInfo, success, failure) {
        //         var xhr, formData;
        //
        //         xhr = new XMLHttpRequest();
        //         xhr.withCredentials = false;
        //         xhr.open('POST', ajax_url);
        //
        //         xhr.onload = function() {
        //             var json;
        //
        //             if (xhr.status != 200) {
        //                 failure('HTTP Error: ' + xhr.status);
        //                 return;
        //             }
        //
        //             json = JSON.parse(xhr.responseText);
        //
        //             if (!json || typeof json.location != 'string') {
        //                 failure('Invalid JSON: ' + xhr.responseText);
        //                 return;
        //             }
        //
        //             success(json.location);
        //         };
        //
        //         formData = new FormData();
        //         formData.append('action', "upload_image");
        //         formData.append('file', blobInfo.blob(), blobInfo.filename());
        //
        //         xhr.send(formData);
        //     },
        //     image_dimensions: true,
        //     image_class_list: [
        //         {title: 'Aucune', value: ''},
        //         {title: 'Image Responsive', value: 'img-responsive'}
        //     ],
        //     style_formats: [
        //         {title: 'Headers', items: [
        //                 {title: 'h1', block: 'h1'},
        //                 {title: 'h2', block: 'h2'},
        //                 {title: 'h3', block: 'h3'},
        //                 {title: 'h4', block: 'h4'},
        //                 {title: 'h5', block: 'h5'},
        //                 {title: 'h6', block: 'h6'}
        //             ]},
        //
        //         {title: 'Blocks', items: [
        //                 {title: 'p', block: 'p'},
        //                 {title: 'div', block: 'div', styles: {
        //                         'float' : 'left',
        //                         'width': '100%'
        //                     }},
        //                 {title: 'pre', block: 'pre'}
        //             ]},
        //
        //         {title: 'Containers', items: [
        //                 {title: 'section', block: 'section', wrapper: true, merge_siblings: false},
        //                 {title: 'article', block: 'article', wrapper: true, merge_siblings: false},
        //                 {title: 'blockquote', block: 'blockquote', wrapper: true},
        //                 {title: 'hgroup', block: 'hgroup', wrapper: true},
        //                 {title: 'aside', block: 'aside', wrapper: true},
        //                 {title: 'figure', block: 'figure', wrapper: true}
        //             ]},
        //         {title: 'Float', items: [
        //                 {title: 'Float Left Image', selector: 'img', styles: {
        //                         'float' : 'left',
        //                         'margin-right': '20px'
        //                     }},
        //                 {title: 'Float Right Image', selector: 'img', styles: {
        //                         'float' : 'right',
        //                         'margin-left': '20px'
        //                     }},
        //                 {title: 'Float Left Texte', selector: 'p', styles: {
        //                         'float' : 'left',
        //                         'margin': '0 10px'
        //                     }},
        //                 {title: 'Float Right Texte', selector: 'p', styles: {
        //                         'float' : 'right',
        //                         'margin': '0 10px'
        //                     }},
        //             ]},
        //     ]
        //
        // });
    });
}