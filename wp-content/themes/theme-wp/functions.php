<?php
include_once( 'inc/theme-options.php' );
include_once( 'inc/walker_comments.php' );
include_once( 'inc/global_functions.php' );
include_once( 'inc/customfunctions.php' );
include_once( 'inc/posttype.php' );

function theme_name_setup(){
	$domain = 'tooeasy';
	load_theme_textdomain( $domain, get_template_directory() . '/languages' );
}
add_action( 'after_setup_theme', 'theme_name_setup' );

if (function_exists('add_theme_support')) {
    add_theme_support('post-thumbnails');
}   
if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'subslider', 150, 100, true );
}

$wnm_custom = array( 'stylesheet_directory_uri' => get_stylesheet_directory_uri() );
wp_localize_script( 'custom-js', 'directory_uri', $wnm_custom );

function add_theme_scripts() {  
    wp_enqueue_script('media-upload');
    wp_enqueue_script( 'bundle', get_template_directory_uri() . '/assets/js/bundle.js', array( 'jquery' ), false, true );
    wp_enqueue_style( 'bundle', get_template_directory_uri() . '/assets/css/bundle.css' );

    $wnm_custom = array( 'ajax_url' => admin_url( 'admin-post.php' ));
    wp_localize_script( 'bundle', 'directory_uri', $wnm_custom );
}
add_action('wp_enqueue_scripts', 'add_theme_scripts');

add_action( 'current_screen', 'wpse113256_this_screen' );
function wpse113256_this_screen() {
    $current_screen = get_current_screen();
    if( $current_screen->id === "appearance_page_theme_options" ) {
        if ( !did_action( 'wp_enqueue_media' ) ){
            wp_enqueue_media();
        }
    }
}

function get_css(){
    $css = '';
    if(is_home() || is_page(51)):
        $css .= 'accueil';
    elseif(is_page()):
        $css .= 'page';
    else:
        $css .= 'page';
    endif;
    
    return $css;
}

// This theme uses wp_nav_menu() in two locations.
    register_nav_menus( array(
            'primary' => __( 'Primary Menu' ),
            'footer_1' => __( 'Menu Footer 1' ),
            'footer_2' => __( 'Menu Footer 2' ),

) );
function themename_custom_logo_setup() {
    $defaults = array(
        'height'      => 99,
        'width'       => 239,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    );
    add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'themename_custom_logo_setup' );
add_theme_support( 'custom-logo' );


function widgets_init() { 
    register_sidebar( array(
        'name' => 'Header Droit',
        'id' => 'widget-headright',
        'before_widget' => '<div class="header-left-widget polylang-widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="header-widget-title">',
        'after_title' => '</p>',
    ));
    register_sidebar( array(
        'name' => 'Footer Gauche',
        'id' => 'widget-footleft',
        'before_widget' => '<div class="footer-left-widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="footer-widget-title">',
        'after_title' => '</p>',
    ));
    register_sidebar( array(
        'name' => 'Footer Droit',
        'id' => 'widget-footright',
        'before_widget' => '<div class="footer-right-widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="footer-widget-title">',
        'after_title' => '</p>',
    ));
}
add_action( 'widgets_init', 'widgets_init' );

add_action( 'admin_init', 'hide_editor' );
function hide_editor() {
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
    if( !isset( $post_id ) ) return;
    $pagetitle = get_the_title($post_id);
    if($pagetitle == 'Accueil'){
        remove_post_type_support('page', 'editor');
    }
}

add_action( 'user_register', 'save_pref_categories', 10, 1 );

function save_pref_categories( $user_id ) {

    if(isset($_POST['categories'])){

        setSavedCategories($_POST, $user_id);
    }
}

add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
    if (!current_user_can('administrator') && !is_admin()) {
        show_admin_bar(false);
    }
}

function post_published_notification( $ID, $post ) {

    global $wpdb;

    $terms = wp_get_post_terms( $ID, "category");

    $result = array();

    foreach ($terms as $term){

        $users = $wpdb->get_results("SELECT * FROM drome_categories_users WHERE id_categorie=".$term->term_id);
        foreach ($users as $user) {
            if(!in_array($user->id_user, $result)){

                $result[] = $user->id_user;
            }
        }
    }



    foreach($result as $res)
    {

        $u = get_user_by("ID", $res);

        if($u != null)
        {
            sendMail($u, $post);
        }

    }

}
add_action( 'publish_post', 'post_published_notification', 10, 2 );

function sendMail($user, $post)
{
    ob_start();
    include(get_template_directory(). '/templates/emails/notifications.php');
    $email_content = ob_get_contents();
    ob_end_clean();
    $headers = array('Content-Type: text/html; charset=UTF-8');

    $terms = wp_get_post_terms(  $post->ID, "category");
    $title = $post->post_title;
    $permalink = get_permalink( $post->ID );

    $email_content = str_replace("{{categorie}}", $terms[0]->name, $email_content);
    $email_content = str_replace("{{titre_article}}", $title, $email_content);
    $email_content = str_replace("{{lien_article}}", $permalink, $email_content);

    $to[] = sprintf( '%s <%s>', $user->data->first_name." ".$user->data->last_name, $user->data->user_email );
    $subject = sprintf( 'Nouvel article dans la catégorie %s sur le site La Drôme - Espace collectivités ', $terms[0]->name);
    wp_mail( $to, $subject, $email_content, $headers );
}

add_action( 'admin_post_upload_image', 'upload_image' );

function upload_image()
{
    $accepted_origins = array("http://localhost", "http://46.105.88.154", "http://ladrome.recettage.fr");

    reset ($_FILES);
    $temp = current($_FILES);
    if (is_uploaded_file($temp['tmp_name'])){
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            // same-origin requests won't set an origin. If the origin is set, it must be valid.
            if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
                header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
            } else {
                header("HTTP/1.1 403 Origin Denied");
                return;
            }
        }

        $id_image = media_handle_upload( 'file', 0 );

        if($id_image != null) {
            echo json_encode(array('location' => wp_get_attachment_url($id_image)));
        } else {
            header("HTTP/1.1 500 Server Error");
        }

    } else {
        // Notify editor that the upload failed
        header("HTTP/1.1 500 Server Error");
    }
}

add_action( 'admin_post_subcribeMailPoet', 'subcribeMailPoet' );
function subcribeMailPoet()
{
    $accepted_origins = array("http://localhost", "http://46.105.88.154", "http://ladrome.recettage.fr");

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        // same-origin requests won't set an origin. If the origin is set, it must be valid.
        if (in_array($_SERVER['HTTP_ORIGIN'], $accepted_origins)) {
            header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        } else {
            header("HTTP/1.1 403 Origin Denied");
            return;
        }
    }


    if (class_exists(\MailPoet\API\API::class)) {
        $mailpoet_api = \MailPoet\API\API::MP('v1');
    } else {
        header("HTTP/1.1 500 Server Error");
    }

    $email = $_POST['email'];

    $user_data = array(
        'email' => $email,
    );

    $user_list = array(3);

    try {
        $mailpoet_api->addSubscriber($user_data, $user_list);

        echo json_encode(array("success" => true, "erreur" => null));


    } catch (Exception $e) {
        echo json_encode(array("erreur" => $e->getMessage(), "success" => false));
    }
}

function mod_contact7_form_content( $template, $prop ) {
    if ( "form" == $prop ) {
        return implode( '', array(
            '<div class="identite row justify-content-between">',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Prénom*</label>',
            '[text* prenom]',
            '</div>',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Nom*</label>',
            '[text* nom]',
            '</div>',
            '</div>',
            '<div class="adresse row justify-content-between">',
            '<div class="input col-12 d-flex flex-column">',
            '<label>Adresse</label>',
            '[text adresse]',
            '</div>',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Code postal</label>',
            '[text code_postal]',
            '</div>',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Ville</label>',
            '[text ville]',
            '</div>',
            '</div>',
            '<div class="coordonnees row justify-content-between">',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Téléphone*</label>',
            '[tel* telephone]',
            '</div>',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Email*</label>',
            '[email* email]',
            '</div>',
            '</div>',
            '<div class="structure row justify-content-between">',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Structure</label>',
            '[text* structure]',
            '</div>',
            '<div class="input col-xl-6 d-flex flex-column">',
            '<label>Fonction</label>',
            '[text* fonction]',
            '</div>',
            '</div>',
            '<div class="message d-flex flex-column">',
            '<label>Message*</label>',
            '[textarea message]',
            '</div>',
            '<div class="col-12">',
            '<label for="rgpd">',
            '[checkbox* rgpd ""]',
            'J\'accepte les termes et conditions de la politique de confidentialité. <a title="Politique de confidentialité La Drôme - Espace collectivités" href="'.get_the_permalink(3).'">Lire notre Politique de confidentialité</a>.',
            '</label>',
            '</div>',
            '<div class="submit">',
            '[submit class:btnvalider class:white class:centeredtext "Envoyer"]',
            '</div>'
        ) );
    } else {
        return $template;
    }
}add_filter(
    "wpcf7_default_template",
    "mod_contact7_form_content",
    10,
    2
);

function mod_contact7_form_title( $template ) {
    $template->set_title( 'Contact' );
  return $template;
}add_filter(
    'wpcf7_contact_form_default_pack',
    'mod_contact7_form_title'
);


