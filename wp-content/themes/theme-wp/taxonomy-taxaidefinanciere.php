<?php
get_header();
$options = get_option('options');
$term = get_term(get_queried_object()->term_id, 'taxaidefinanciere');
$term_parent = get_term(get_queried_object()->parent);
$args = array(
    'post_type' => 'aidefinanciere',
    'posts_per_page' => '-1',
    'tax_query' => array(
        array(
            'taxonomy' => 'taxaidefinanciere',
            'field' => 'slug',
            'terms' => $term->slug
        )
    ),
    'paged' => $paged
);
$loop_service = new WP_Query($args);
$id_user = wp_get_current_user()->data->ID;
$entete = get_field("image", $term) != ""? get_field("image", $term) : asset_url("img/entete.jpg");
?>

<section class="fiche-aide-financiere">
    <div class="btnretour">
        <i class="icon-full_arrow_left"></i>
        <a href="<?= get_the_permalink($options['lien_aide'])."#".$term_parent->slug?>">Retour</a>
    </div>

    <div class="ariane d-flex flex-wrap align-items-center">
        <a href="index.php">Accueil</a>
        <span class="arrow">></span>
        <a href="<?= get_the_permalink($options['lien_aide_financiere'])?> ">Aide financière</a>
        <span class="arrow">></span>
        <a href="<?= get_the_permalink($options['lien_aide'])?>">Aides départementales aux collectivités et aux tiers</a>
        <span class="arrow">></span>
        <a href="<?= get_the_permalink($options['lien_aide'])."#".$term_parent->slug?> "><?= $term_parent->name?></a>
        <span class="arrow">></span>
        <p><?= $term->name?></p>
    </div>

    <div class="topfiche" style="background-image: url('<?= $entete?>')">
        <div class="image"></div>
        <div class="bloctitre upper">
            <h1 class="upper"><?= $term->name?></h1>
            <div class="paraf">
                <?=$term->description?>
            </div>
        </div>
    </div>


    <?php if($loop_service->have_posts()):?>
        <div class="contenu-aide-financiere centeredtext">
    <?php
    while ( $loop_service->have_posts() ) : $loop_service->the_post(); ?>
        <?php if(get_field("accessible")){
            continue;
        }?>
        <button class="accordeon hide-print">
            <p><?php the_title()?></p>
        </button>
        <div class="panelaccordeon hide-print">
            <div class="d-flex align-items-center">
                <p class="update upper">mise à jour le <?= the_modified_date()?> - </p>
                <a href="" onclick="javascript:window.print()" class="btnimprimer d-flex centeredtext align-items-center">
                    <i class="icon-print"></i>
                    <p>Imprimer</p>
                </a>
                <?php if(user_can( $id_user, 'administrator' )):?>
                <a target="_blank" href="<?= get_edit_post_link()?>" class="btnimprimer d-flex centeredtext align-items-center">
                    <span class="dashicons dashicons-admin-tools"></span>
                </a>
                <?php endif;?>
            </div>
            <div class="cms" style="padding-top: 15px">
                <?php the_content()?>
            </div>
        </div>
        <hr>
    <?php endwhile;?>
        </div>
    <?php else:?>
    <div class="contenu-aide-financiere centeredtext">
        Pas de fiche pour cette catégorie
    </div>
    <?php endif;?>
</section>
<?php

get_footer();
?>
