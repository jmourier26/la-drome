<?php

// Register Custom Post Type Service
function create_service_cpt() {

    $labels = array(
        'name' => _x( 'Services', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Service', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Assistance technique', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Service', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Service', 'textdomain' ),
        'attributes' => __( 'Attributs Service', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Service:', 'textdomain' ),
        'all_items' => __( 'Tous les services', 'textdomain' ),
        'add_new_item' => __( 'Ajouter nouvel Service', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvel Service', 'textdomain' ),
        'edit_item' => __( 'Modifier Service', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Service', 'textdomain' ),
        'view_item' => __( 'Voir Service', 'textdomain' ),
        'view_items' => __( 'Voir Services', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Service', 'textdomain' ),
        'not_found' => __( 'Aucun Service trouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Service trouvé dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Service', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cet Service', 'textdomain' ),
        'items_list' => __( 'Liste Services', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Services', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Services', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Service', 'textdomain' ),
        'description' => __( 'Services proposés par le département', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-admin-tools',
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'service', $args );

}
add_action( 'init', 'create_service_cpt', 0 );

// Register Custom Post Type Document de reference
function create_documentdereference_cpt() {

    $labels = array(
        'name' => _x( 'Documents de références', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Document de référence', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Documents de references', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Document de reference', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Document de reference', 'textdomain' ),
        'attributes' => __( 'Attributs Document de reference', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Document de reference:', 'textdomain' ),
        'all_items' => __( 'Tous Documents de references', 'textdomain' ),
        'add_new_item' => __( 'Ajouter un nouveau document de référence', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouveau Document de reference', 'textdomain' ),
        'edit_item' => __( 'Modifier Document de reference', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Document de reference', 'textdomain' ),
        'view_item' => __( 'Voir Document de reference', 'textdomain' ),
        'view_items' => __( 'Voir Documents de references', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Document de reference', 'textdomain' ),
        'not_found' => __( 'Aucun Document de reference trouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Document de reference trouvé dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Document de reference', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur ce document de reference', 'textdomain' ),
        'items_list' => __( 'Liste Documents de references', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Documents de references', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Documents de references', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Document de reference', 'textdomain' ),
        'description' => __( 'Liste des documents de référence', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-portfolio',
        'supports' => array('title', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'documentdereference', $args );

}
add_action( 'init', 'create_documentdereference_cpt', 0 );

function create_fichepratique_cpt() {

    $labels = array(
        'name' => _x( 'Fiches pratiques', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Fiche pratique', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Fiches pratiques', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Fiche pratique', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Fiche pratique', 'textdomain' ),
        'attributes' => __( 'Attributs Fiche pratique', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Fiche pratique:', 'textdomain' ),
        'all_items' => __( 'Toutes les Fiches pratiques', 'textdomain' ),
        'add_new_item' => __( 'Ajouter nouvel Fiche pratique', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvel Fiche pratique', 'textdomain' ),
        'edit_item' => __( 'Modifier Fiche pratique', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Fiche pratique', 'textdomain' ),
        'view_item' => __( 'Voir Fiche pratique', 'textdomain' ),
        'view_items' => __( 'Voir Fiches pratiques', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Fiche pratique', 'textdomain' ),
        'not_found' => __( 'Aucun Fiche pratique trouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Fiche pratique trouvée dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Fiche pratique', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cette Fiche pratique', 'textdomain' ),
        'items_list' => __( 'Liste Fiches pratiques', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Fiches pratiques', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Fiches pratiques', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Fiche pratique', 'textdomain' ),
        'description' => __( 'Fiches pratiques du département de la Drôme', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-media-text',
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'fichepratique', $args );

}
add_action( 'init', 'create_fichepratique_cpt', 0 );


// Register Taxonomy Assistance technique
function create_assistancetechnique_tax() {

    //Page Assistance technique
    $path = str_replace(home_url(),'',get_permalink(26));

    if(substr($path, -1) == '/') {
        $path = substr($path, 0, -1);
        $path = ltrim($path, '/');
    }


    $labels = array(
        'name'              => _x( 'Assistances techniques', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Assistance technique', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Rechercher dans l\'assistance technique', 'textdomain' ),
        'all_items'         => __( 'Toutes les catégories de services', 'textdomain' ),
        'parent_item'       => __( 'Catégorie parente', 'textdomain' ),
        'parent_item_colon' => __( 'Catégorie parente:', 'textdomain' ),
        'edit_item'         => __( 'Editer l\'assistance technique', 'textdomain' ),
        'update_item'       => __( 'Mettre à jour l\'assistance technique', 'textdomain' ),
        'add_new_item'      => __( 'Ajouter une nouvelle catégorie', 'textdomain' ),
        'new_item_name'     => __( 'Nom de la catégorie', 'textdomain' ),
        'menu_name'         => __( 'Assistance technique', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Catégorie de service', 'textdomain' ),
        'hierarchical' => true,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'rewrite'  => array( 'slug' => ($path != null)? $path : "assistance-technique" , 'with_front' => false),
    );
    register_taxonomy( 'assistancetechnique', array("service", "documentdereference", "fichepratique"), $args );

}
add_action( 'init', 'create_assistancetechnique_tax' );

// Register Custom Post Type Aide financiére
function create_aidefinancire_cpt() {

    $labels = array(
        'name' => _x( 'Aides financières', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Aide financière', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Aides financières', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Aide financière', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Aide financière', 'textdomain' ),
        'attributes' => __( 'Attributs Aide financière', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Aide financière:', 'textdomain' ),
        'all_items' => __( 'Toutes les aides financieres', 'textdomain' ),
        'add_new_item' => __( 'Ajouter une nouvelle Aide financière', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvelle Aide financière', 'textdomain' ),
        'edit_item' => __( 'Modifier Aide financière', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Aide financière', 'textdomain' ),
        'view_item' => __( 'Voir Aide financière', 'textdomain' ),
        'view_items' => __( 'Voir Aides financieres', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Aides financières', 'textdomain' ),
        'not_found' => __( 'Aucun Aide financière trouvée.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Aide financière trouvée dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Aide financière', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cette Aide financière', 'textdomain' ),
        'items_list' => __( 'Liste Aides financieres', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Aides financieres', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Aides financieres', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Aide financière', 'textdomain' ),
        'description' => __( 'Fiches pratiques du département de la Drôme', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-heart',
        'supports' => array('title', 'editor'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => false,
        'hierarchical' => true,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'aidefinanciere', $args );

}
add_action( 'init', 'create_aidefinancire_cpt', 0 );

// Register Taxonomy Catégorie aide financière
function create_catgorieaidefinancire_tax() {

    //Page Aides départementales aux collectivités et aux tiers
    $path = str_replace(home_url(),'',get_permalink(105));

    if(substr($path, -1) == '/') {
        $path = substr($path, 0, -1);
        $path = ltrim($path, '/');

    }

    $labels = array(
        'name'              => _x( 'Catégories aides financières', 'taxonomy general name', 'textdomain' ),
        'singular_name'     => _x( 'Catégorie aide financière', 'taxonomy singular name', 'textdomain' ),
        'search_items'      => __( 'Search Catégories aides financières', 'textdomain' ),
        'all_items'         => __( 'All Catégories aides financières', 'textdomain' ),
        'parent_item'       => __( 'Parent Catégorie aide financière', 'textdomain' ),
        'parent_item_colon' => __( 'Parent Catégorie aide financière:', 'textdomain' ),
        'edit_item'         => __( 'Edit Catégorie aide financière', 'textdomain' ),
        'update_item'       => __( 'Update Catégorie aide financière', 'textdomain' ),
        'add_new_item'      => __( 'Add New Catégorie aide financière', 'textdomain' ),
        'new_item_name'     => __( 'New Catégorie aide financière Name', 'textdomain' ),
        'menu_name'         => __( 'Catégorie aide financière', 'textdomain' ),
    );
    $args = array(
        'labels' => $labels,
        'description' => __( 'Catégorie aide financière', 'textdomain' ),
        'hierarchical' => true,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'show_in_nav_menus' => true,
        'show_tagcloud' => true,
        'show_in_quick_edit' => true,
        'show_admin_column' => true,
        'show_in_rest' => true,
        'rewrite'  => array( 'slug' => ($path != null)? $path: "aides-financieres" , 'with_front' => false),
        //'rewrite' => "aides-financieres"
    );
    register_taxonomy( 'taxaidefinanciere', array("aidefinanciere"), $args );

}
add_action( 'init', 'create_catgorieaidefinancire_tax' );

// Register Custom Post Type Lien utile
function create_lienutile_cpt() {

    $labels = array(
        'name' => _x( 'Liens utiles', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Lien utile', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Liens utiles', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Lien utile', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Lien utile', 'textdomain' ),
        'attributes' => __( 'Attributs Lien utile', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Lien utile:', 'textdomain' ),
        'all_items' => __( 'Tous les liens utiles', 'textdomain' ),
        'add_new_item' => __( 'Ajouter nouvel Lien utile', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvel Lien utile', 'textdomain' ),
        'edit_item' => __( 'Modifier Lien utile', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Lien utile', 'textdomain' ),
        'view_item' => __( 'Voir Lien utile', 'textdomain' ),
        'view_items' => __( 'Voir Liens utiles', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Lien utile', 'textdomain' ),
        'not_found' => __( 'Aucun Lien utile trouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Lien utile trouvé dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Lien utile', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cet Lien utile', 'textdomain' ),
        'items_list' => __( 'Liste Liens utiles', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Liens utiles', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Liens utiles', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Lien utile', 'textdomain' ),
        'description' => __( 'Liens utiles aux utilisateurs du site', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-external',
        'supports' => array('title',  'editor', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'lienutile', $args );

}
add_action( 'init', 'create_lienutile_cpt', 0 );

// Register Custom Post Type Date clé
function create_datecl_cpt() {

    $labels = array(
        'name' => _x( 'Dates clés', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Date clé', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Dates clés', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Date clé', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Date clé', 'textdomain' ),
        'attributes' => __( 'Attributs Date clé', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Date clé:', 'textdomain' ),
        'all_items' => __( 'Tous Dates clés', 'textdomain' ),
        'add_new_item' => __( 'Ajouter nouvel Date clé', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvel Date clé', 'textdomain' ),
        'edit_item' => __( 'Modifier Date clé', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Date clé', 'textdomain' ),
        'view_item' => __( 'Voir Date clé', 'textdomain' ),
        'view_items' => __( 'Voir Dates clés', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Date clé', 'textdomain' ),
        'not_found' => __( 'Aucun Date clétrouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Date clétrouvé dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Date clé', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cet Date clé', 'textdomain' ),
        'items_list' => __( 'Liste Dates clés', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Dates clés', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Dates clés', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Date clé', 'textdomain' ),
        'description' => __( 'Dates clés', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-calendar',
        'supports' => array('title', 'editor', 'thumbnail'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'datescles', $args );

}
add_action( 'init', 'create_datecl_cpt', 0 );

// Register Custom Post Type Partenaire
function create_partenaire_cpt() {

    $labels = array(
        'name' => _x( 'Partenaires', 'Post Type General Name', 'textdomain' ),
        'singular_name' => _x( 'Partenaire', 'Post Type Singular Name', 'textdomain' ),
        'menu_name' => _x( 'Partenaires', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar' => _x( 'Partenaire', 'Add New on Toolbar', 'textdomain' ),
        'archives' => __( 'Archives Partenaire', 'textdomain' ),
        'attributes' => __( 'Attributs Partenaire', 'textdomain' ),
        'parent_item_colon' => __( 'Parents Partenaire:', 'textdomain' ),
        'all_items' => __( 'Tous les Partenaires', 'textdomain' ),
        'add_new_item' => __( 'Ajouter nouveau Partenaire', 'textdomain' ),
        'add_new' => __( 'Ajouter', 'textdomain' ),
        'new_item' => __( 'Nouvel Partenaire', 'textdomain' ),
        'edit_item' => __( 'Modifier Partenaire', 'textdomain' ),
        'update_item' => __( 'Mettre à jour Partenaire', 'textdomain' ),
        'view_item' => __( 'Voir le Partenaire', 'textdomain' ),
        'view_items' => __( 'Voir les Partenaires', 'textdomain' ),
        'search_items' => __( 'Rechercher dans les Partenaire', 'textdomain' ),
        'not_found' => __( 'Aucun Partenaire trouvé.', 'textdomain' ),
        'not_found_in_trash' => __( 'Aucun Partenaire trouvé dans la corbeille.', 'textdomain' ),
        'featured_image' => __( 'Image mise en avant', 'textdomain' ),
        'set_featured_image' => __( 'Définir l’image mise en avant', 'textdomain' ),
        'remove_featured_image' => __( 'Supprimer l’image mise en avant', 'textdomain' ),
        'use_featured_image' => __( 'Utiliser comme image mise en avant', 'textdomain' ),
        'insert_into_item' => __( 'Insérer dans Partenaire', 'textdomain' ),
        'uploaded_to_this_item' => __( 'Téléversé sur cet Partenaire', 'textdomain' ),
        'items_list' => __( 'Liste Partenaires', 'textdomain' ),
        'items_list_navigation' => __( 'Navigation de la liste Partenaires', 'textdomain' ),
        'filter_items_list' => __( 'Filtrer la liste Partenaires', 'textdomain' ),
    );
    $args = array(
        'label' => __( 'Partenaire', 'textdomain' ),
        'description' => __( 'Partenaires du département', 'textdomain' ),
        'labels' => $labels,
        'menu_icon' => 'dashicons-heart',
        'supports' => array('title', 'thumbnail', 'editor'),
        'taxonomies' => array(),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'hierarchical' => false,
        'exclude_from_search' => false,
        'show_in_rest' => true,
        'publicly_queryable' => true,
        'capability_type' => 'post',
    );
    register_post_type( 'partenaire', $args );

}
add_action( 'init', 'create_partenaire_cpt', 0 );