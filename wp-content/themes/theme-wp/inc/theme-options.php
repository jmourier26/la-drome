<?php
	function register_settings() {
		register_setting( 'theme_options',
			'options', 'validate_options' );
	}
	add_action( 'admin_init', 'register_settings' );
	$options = array(
		'facebook' => '',
	);
	$settings = get_option( 'options', $options );
	function theme_options() {
		add_theme_page( 'Options du Site', 'Options du Site',
		'edit_theme_options', 'theme_options',
		'theme_options_page' );
	}
	add_action( 'admin_menu', 'theme_options' );
	// Construction et affichage de la page et du formulaire
	function theme_options_page() {
	    // On inclut nos tableaux globaux
	    global $options, $barre;
	    
	    // Valide la soumission du formulaire
	    if ( ! isset( $_REQUEST['settings-updated'] ) )
	        $_REQUEST['settings-updated'] = false;
	    ?>
	    <div class="wrap">
	    <?php
	    // Affiche le nom de la page et son icone si celle-ci a été définie
	    screen_icon();
	    echo "<h2>" . wp_get_theme() . __( ' Options du Site' ) . "</h2>";
	    ?>
	    <?php
	    // Si le formulaire vient juste d'etre soumis, affiche une notification
	    if ( false !== $_REQUEST['settings-updated'] ) : ?>
		    <div class="updated fade">
		    	<p><strong><?php _e( 'Options sauvegardées' ); ?></strong></p>
		    </div>
	    <?php endif;?>
	    <form method="post" action="options.php">
	    <?php $settings = get_option( 'options', $options ); ?>
	    <?php
		    // Cette fonction retourne plusieurs champs cachés requis par le formulaire,
		    // parmis lesquels un nonce ("number used once"), un nombre unique utilisé
		    // pour s'assurer que le formulaire n'ait été envoyé que depuis la page
		    // d'administration. Très important pour la sécurité.
		    settings_fields( 'theme_options' );
	    ?>
                <H2>Réseaux Sociaux</H2>
                <table class="form-table options-template">
                    <tr valign="top">
                        <th scope="row">
                            <label for="facebook">Facebook : </label>
                        </th>
                        <td>
                                <input id="nom" name="options[facebook]" type="text" style="width:50%" 
                                    value="<?php  esc_attr_e($settings['facebook']); ?>" />
                        </td>
                    </tr>  
                    <tr valign="top">
                        <th scope="row">
                            <label for="instagram">Instagram : </label>
                        </th>
                        <td>
                                <input id="nom" name="options[instagram]" type="text" style="width:50%" 
                                    value="<?php  esc_attr_e($settings['instagram']); ?>" />
                        </td>
                    </tr>  
                    <tr valign="top">
                        <th scope="row">
                            <label for="twitter">Twitter : </label>
                        </th>
                        <td>
                                <input id="nom" name="options[twitter]" type="text" style="width:50%" 
                                    value="<?php  esc_attr_e($settings['twitter']); ?>" />
                        </td>
                    </tr>  
                </table>
            <H2>Coordonnées</H2>
            <table class="form-table options-template">
                <tr valign="top">
                    <th scope="row">
                        <label for="lieu">Lieu : </label>
                    </th>
                    <td>
                        <input id="lieu" name="options[lieu]" type="text" style="width:50%"
                               value="<?php  esc_attr_e($settings['lieu']); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="adresse">Adresse : </label>
                    </th>
                    <td>
                        <input id="adresse" name="options[adresse]" type="text" style="width:50%"
                               value="<?php  esc_attr_e($settings['adresse']); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="twitter">Code postal : </label>
                    </th>
                    <td>
                        <input id="code_postal" name="options[code_postal]" type="text" style="width:50%"
                               value="<?php  esc_attr_e($settings['code_postal']); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="twitter">Ville : </label>
                    </th>
                    <td>
                        <input id="ville" name="options[ville]" type="text" style="width:50%"
                               value="<?php  esc_attr_e($settings['ville']); ?>" />
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="twitter">Téléphone : </label>
                    </th>
                    <td>
                        <input id="telephone" name="options[telephone]" type="text" style="width:50%"
                               value="<?php  esc_attr_e($settings['telephone']); ?>" />
                    </td>
                </tr>
            </table>
            <H2>Paramètres</H2>
            <table class="form-table options-template">
                <tr valign="top">
                    <th scope="row">
                        <label for="text_aides">Textes aide financière : </label>
                    </th>
                    <td>
                        <textarea name="options[text_aides]" id="text_aides" cols="30" rows="10"  style="width:50%"><?php  esc_attr_e($settings['text_aides']); ?></textarea>
                    </td>
                </tr>
                <?php $pages = get_pages();?>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_aide">Lien aides départementales : </label>
                    </th>
                    <td>
                        <select name="options[lien_aide]" id="lien_aide">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_aide'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_aide_financiere">Lien aide financière : </label>
                    </th>
                    <td>
                        <select name="options[lien_aide_financiere]" id="lien_aide">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_aide_financiere'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_aide">Lien assistance technique : </label>
                    </th>
                    <td>
                        <select name="options[lien_assistance]" id="lien_aide">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_assistance'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="liens_utiles">Lien page liens utiles: </label>
                    </th>
                    <td>
                        <select name="options[liens_utiles]" id="liens_utiles">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['liens_utiles'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_compte">Lien page mon Compte: </label>
                    </th>
                    <td>
                        <select name="options[lien_compte]" id="lien_compte">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_compte'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_creation_compte">Lien page Création compte: </label>
                    </th>
                    <td>
                        <select name="options[lien_creation_compte]" id="lien_creation_compte">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_creation_compte'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_mdp">Lien mot de passe oublié: </label>
                    </th>
                    <td>
                        <select name="options[lien_mdp]" id="lien_mdp">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_mdp'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_mdp">Lien informations personnelles: </label>
                    </th>
                    <td>
                        <select name="options[lien_infos]" id="lien_infos">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_infos'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_contact">Lien page Contact: </label>
                    </th>
                    <td>
                        <select name="options[lien_contact]" id="lien_contact">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_contact'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_mentions">Lien page Mentions légales: </label>
                    </th>
                    <td>
                        <select name="options[lien_mentions]" id="lien_mentions">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_mentions'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_plan">Lien Plan du site: </label>
                    </th>
                    <td>
                        <select name="options[lien_plan]" id="lien_plan">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_plan'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row">
                        <label for="lien_politique">Lien Politique de confidentialité: </label>
                    </th>
                    <td>
                        <select name="options[lien_politique]" id="lien_politique">
                            <option value="0">#</option>
                            <?php foreach ($pages as $page):?>
                                <option <?= ($settings['lien_politique'] == $page->ID)? "selected='selected'" : ""?> value="<?= $page->ID?>"><?= $page->post_title?></option>
                            <?php endforeach;?>
                        </select>
                    </td>
                </tr>
            </table>
                <p class="submit"><input type="submit" class="button-primary"
                    value="Enregistrer" /></p>
	    </form>
	    </div>

	    <script>
                jQuery( document ).ready( function( $ ) {
                    // on gére l'image d'accueil
                    var img_week = "<?= $settings['accueil_image']; ?>";
                    $( '#accueil_image_preview' ).attr( 'src', img_week );
                    $( '#accueil_image' ).val( img_week );

                    $( "#upload_accueil_image" ).on( "click", "#accueil_image_button", function(e) {
                        e.preventDefault();
                        var btnClicked = $( this );
                        var custom_uploader = wp.media({
                            multiple: false 
                        })
                        .on('select', function() {
                            var attachment = custom_uploader.state().get('selection').first().toJSON();
                            $( '#accueil_image' ).val( attachment.url );
                            $( '#accueil_image_preview' ).attr( 'src', attachment.url ).css( 'width', 'auto' );
                        })
                        .open();
                    });
                });
	    </script>
	    <?php
	}
	function validate_options( $input ) {
	    global $options;
	    $settings = get_option( 'options', $options );
	    // Afin d'éviter des vulnérabilités comme XSS, on enlève toute balise HTML
	    $input['facebook'] = wp_filter_nohtml_kses( $input['facebook'] );
	    return $input;
	}
?>