<?php

function url_timthumb($url, $width = 150,$height = 150, $crop = "1", $quality = 80, $align = 't')
{
    $image = get_template_directory_uri() . '/timthumb.php?src='.$url.'&h='.$height.'&w='.$width.'&zc='.$crop.'&q='.$quality.'&a='.$align;

    return $image;
}
function img_timthumb($id_img, $width = 150,$height = 150, $crop = "1", $class = '', $align = 't', $quality = 80){
    $ob_img = new stdClass();
    $ob_img->ID = $id_img;
    
    $url = wp_get_attachment_image_src($ob_img->ID,'large');
    $image_alt = get_post_meta( $ob_img->ID, '_wp_attachment_image_alt', true);
    $img_url = url_timthumb($url[0],$width,$height,$crop,$quality,$align);
    
    return '<img src="'.$img_url.'" alt="'.$image_alt.'" title="'.$ob_img->post_title.'" class="'.$class.'"/>';
}

function get_media_object($id = ''){
    if($id != ''):
        return get_post($id);
    endif;
    
    return null;
}

function  manual_excerpt($str, $startPos=0, $maxLength=200, $cut = ' [...]') {
    $str = strip_tags($str);
    if(strlen($str) > $maxLength) {
        $excerpt   = substr($str, $startPos, $maxLength-3);
        $lastSpace = strrpos($excerpt, ' ');
        $excerpt   = substr($excerpt, 0, $lastSpace);
        $excerpt  .= $cut;
    } else {
        $excerpt = $str;
    }
    
    return $excerpt;
}

function debug($item){
    echo "<pre>";
    var_dump($item);
    echo "</pre>";
}

function get_category_link_by_slug($slug){
    $cat = get_category_by_slug($slug);
    
    return get_category_link($cat);
}

function get_last_article_by_slug($slug = ''){
    $args= array(
	'numberposts' => 1,
        'post_type' => 'post',
        'tax_query' => array(                     //(array) - use taxonomy parameters (available with Version 3.1).
            'relation' => 'AND',
            array(
                'taxonomy' => 'category',
                'terms' => '"'.$slug.'"',
                'field' => 'slug'
            )
        )
    );
    if(is_category()):
        $category = get_queried_object();
        $args['tax_query'][] = array(
                            'taxonomy' => 'category',
                            'terms' => '"'.$category->slug.'"',
                            'field' => 'slug'
                        );
    endif;
    $array = wp_get_recent_posts($args, OBJECT);
    return $array[0];
}

function custom_get_page($nameorid){
    if(!function_exists('pll_the_languages')) return null;

    $post_id = false;
    if(is_numeric($nameorid)){
            $post_id = $nameorid;
    }else{
            $post = get_page_by_path($nameorid, OBJECT, array('post','page','dossier','document'));
            if($post){
                    $post_id = $post->ID;
            }
    }
    if($post_id){
        $id = $post_id;
    }
    
    return get_post($id);
}

function custom_get_page_link($nameorid){
    if(!function_exists('pll_the_languages')) return site_url($nameorid);

    $post_id = false;
    if(is_numeric($nameorid)){
        $post_id = $nameorid;
    }else{
        $post = get_page_by_path($nameorid, OBJECT, array('post','page','dossier','document'));
        if($post){
                $post_id = $post->ID;
        }
    }
    if($post_id){
        $post_id_lang = pll_get_post($post_id);
        if($post_id_lang){
                return get_permalink($post_id_lang);
        }
        return get_permalink($post_id);
    }else{
        return site_url($nameorid);
    }
}

add_post_type_support( 'page', 'excerpt' );

function base_url($uri = ""){
    $base = get_template_directory_uri();
    return $base.'/'.$uri;
}

function asset_url($file = ""){
    $base = base_url('assets/');
    return $base . $file;
}

function css_url($file = ""){
    $base = base_url('assets/css/');
    return $base . $file;
}

function js_url($file = ""){
    $base = base_url('assets/js/');
    return $base . $file;
}

function img_url($file = ""){
    $base = base_url('assets/images/');
    return $base . $file;
}

function get_nav_menu_items_by_location( $location, $args = [] ) {

    // Get all locations
    $locations = get_nav_menu_locations();

    // Get object id by location
    $object = wp_get_nav_menu_object( $locations[$location] );

    // Get menu items by menu name
    $menu_items = wp_get_nav_menu_items( $object->name, $args );

    // Return menu post objects
    return $menu_items;
}