<?php
/**
 * Menu Walker Class which builds the drop down
 **/
class TI_Menu_Responsive extends Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"sub-links dropdown-menu container\">\n";
    }
    function end_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }
    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        $children = get_posts(array('post_type' => 'nav_menu_item', 'nopaging' => true, 'numberposts' => 1, 'meta_key' => '_menu_item_menu_item_parent', 'meta_value' => $item->ID));
        //var_dump($children);
        global $ti_option;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $class_names = $value = '';
        $classes = empty( $item->classes ) ? array() : (array) $item->classes;
        if($children):
            $classes[] = 'dropdown';
        endif;
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
        $class_names = ' class="' . esc_attr( $class_names ) . '"';
        $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names ;
        $output .= '>';
        //$attributes  = ( 'category' == $item->object ) ? ' data-category="'  . esc_attr( $item->url ) .'"' : '';
        $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
        $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
        $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
        $item_output = $args->before;
        if(!empty($children)):
            $item_output .= '<a'. $attributes . '" class="dropdown-toggle" data-toggle="dropdown">';
        else:
            $item_output .= '<a'. $attributes . '">';
        endif;   
        $item_output .= '<span><span>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</span></span>';
        if(!empty($children)):
            $item_output .= '<i class="fa fa-chevron-down" aria-hidden="true"></i>';
        endif;
        $item_output .= '</a>';
        /*f ( $ti_option['site_mega_menu'] == true ) {
            if ( 'category' == $item->object ){
                $item_output  .= '<div class="sub-menu">';
            }
        }*/
        $item_output .= $args->after;
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }
    function end_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
        global $ti_option;
        if ( $ti_option['site_mega_menu'] == true ) {
            if ( 'category' == $item->object ){
                $output .= "</div>\n";
            }
        }
        $output .= "</li>\n";
    }
}
?>