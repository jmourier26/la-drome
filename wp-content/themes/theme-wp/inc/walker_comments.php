<?php
class MY_Walker_Comment extends Walker_Comment {

    function start_lvl(&$output, $depth, $args) {
        $GLOBALS['comment_depth'] = $depth + 1;
        echo '<div class="children col-sm-offset-2 col-sm-10">';
    }

    function end_lvl(&$output, $depth, $args) {
        $GLOBALS['comment_depth'] = $depth + 1;
        echo '</div>';
    }

    function start_el(&$output, $comment, $depth, $args) {
        $depth++;
        $GLOBALS['comment_depth'] = $depth;

        if ( !empty($args['callback']) ) {
            call_user_func($args['callback'], $comment, $args, $depth);
            return;
        }

        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);
        ?>
        <div class="com comtexte lefttext">
            <div class="entetecom d-flex align-items-center">
                <p class="nom"><?= get_comment_author() ?></p>
                <p>-</p>
                <p class="datecom upper">
                    <?= get_comment_date() ?>
                </p>
            </div>
            <div class="contenucom">
                <?php comment_text() ?>
            </div>
        </div>
        <?php
    }

    function end_el(&$output, $comment, $depth, $args) {
        $GLOBALS['comment_depth'] = $depth + 1;
        echo '';
    }
}
?>