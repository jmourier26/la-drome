<?php


function getSavedCategories($user_id)
{
    global $wpdb;
    $resultats = $wpdb->get_results("SELECT * FROM drome_categories_users WHERE id_user =".$user_id) ;

    $result = array();

    foreach ($resultats as $res)
    {
        $result[] = $res->id_categorie;
    }

    return $result;
}

function setSavedCategories($POST, $user_id)
{
    global $wpdb;
    $wpdb->delete("drome_categories_users", array("id_user" => $user_id));

    if(isset($POST['categories'])){

        $categories = $POST['categories'];

        foreach ($categories as $category){

            $term = get_term($category, "category");
            if($term != null){

                $wpdb->insert(
                    'drome_categories_users',
                    array(
                        'id_categorie' => $category,
                        'id_user' => $user_id
                    )
                );
            }
        }
    }
}

/**
 * Returns all child nav_menu_items under a specific parent
 *
 * @param int the parent nav_menu_item ID
 * @param array nav_menu_items
 * @param bool gives all children or direct children only
 * @return array returns filtered array of nav_menu_items
 */
function get_nav_menu_item_children( $parent_id, $nav_menu_items, $depth = true ) {
    $nav_menu_item_list = array();
    foreach ( (array) $nav_menu_items as $nav_menu_item ) {
        if ( $nav_menu_item->menu_item_parent == $parent_id ) {
            $nav_menu_item_list[] = $nav_menu_item;
            if ( $depth ) {
                if ( $children = get_nav_menu_item_children( $nav_menu_item->ID, $nav_menu_items ) )
                    $nav_menu_item_list = array_merge( $nav_menu_item_list, $children );
            }
        }
    }
    return $nav_menu_item_list;
}
